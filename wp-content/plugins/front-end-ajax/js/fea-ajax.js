jQuery(document).ready(function($) {
	
	var removeWarning = function() {
		console.log('hallo');
		$('.warning').fadeOut(200, function() {
			$('.warning').remove();
		});
	};

	// Connect new managers
	$('#fea-connection-manager').submit(function() {
		$('#manager_submit').attr('disabled', true);

		data = {
			action: 'fea_post_connection',
			user: document.getElementById('manager_id').value,
			team: document.getElementById('team_id').value,
			connection_type: 'team_managers',
			fea_nonce: fea_vars.fea_nonce
		};

		if(data.user != ''){

			$.post(fea_vars.ajaxurl, data, function (response) {
				var response = $(response).hide().fadeIn(200)
				$('#managers').append(response);
				$('#add_manager').val('');
				$('#manager_submit').attr('disabled', true);
			});

			return false;
		}
	});

	// Connect new members
	$('#fea-connection-member').submit(function() {
		$('#member_submit').attr('disabled', true);

		data = {
			action: 'fea_post_connection',
			user: document.getElementById('member_id').value,
			team: document.getElementById('team_id').value,
			connection_type: 'team_members',
			fea_nonce: fea_vars.fea_nonce
		};

		if(data.user != ''){

			$.post(fea_vars.ajaxurl, data, function (response) {
				var response = $(response).hide().fadeIn(200);
				$('#members').append(response);
				$('#add_member').val('');
				$('#add_member').removeClass("auto-selected");
			});

			return false;
		} else {
			event.preventDefault();
			$('#members').append('<div class="warning">Please choose a User from the menu</div>');
			setTimeout(removeWarning, 2000);
		}
	});

	// Delete connections
	$(document).on( 'click', 'a.delete-connection', function() {
		user_id = $(this).data('user_id');
		team_id = document.getElementById('team_id').value;
		connection = $(this).parent();
		type = connection.parent().attr("id");
		data = {
			action: 'delete_connection',
			user_id: user_id,
			team_id: team_id,
			type: type,
			fea_nonce: fea_vars.fea_nonce
		};

		$.post(fea_vars.ajaxurl, data, function (response) {
			if( response == 'success' ) {
				connection.fadeOut(200, function() {
					connection.remove();
				});
			}
		});

		return false;
	});

	// Search Autocomplete Members
	$( '#add_member, #add_manager' ).autocomplete({
		minLength: 2,
		source: function( request, response ) {  
			team_id = document.getElementById('team_id').value;
			role = this.element.attr('id');
			jQuery.getJSON( fea_vars.ajaxurl + "?callback=?&action=search_autocomplete&role=" + role + "&team_id=" + team_id, request, response );
		},
		select: function(event, ui) {
		if(role == "add_member") {role_id = "#member_id"; submit = "#member_submit";} else if(role == "add_manager") {role_id = "#manager_id"; submit = "#manager_submit";}
			this.value = ui.item.value;
			if(!$(this).hasClass('auto-selected')) { this.className += this.className ? '  auto-selected' : 'auto-selected'; }; 
			$( role_id ).val(ui.item ? ui.item.id : "");
			$( submit ).attr('disabled', false);
		},
		change: function(event, ui) {
			if(role == "add_member") {role_id = "#member_id"; submit = "#member_submit";} else if(role == "add_manager") {role_id = "#manager_id"; submit = "#manager_submit";}
			if( ui.item ) {
				$( role_id ).val(ui.item.id); 
				$( submit ).attr('disabled', false);
			} else {
				$( role_id ).val('');
				$(this).removeClass("auto-selected");
				$( submit ).attr('disabled', true);
			}
		},
	});

	$.ui.autocomplete.prototype._renderItem = function( ul, item){
		var cls = 'search-highlight';
		var term = this.term.split(' ').join('|');
		var re = new RegExp("(" + term + ")", "gi") ;
		var t = item.label.replace(re,"<span class='" + cls + "'>$1</span>");
		return $( "<li></li>" )
	    	.data( "item.autocomplete", item )
	    	.append( "<a>" + t + "</a>" )
	    	.appendTo( ul );
	};
});