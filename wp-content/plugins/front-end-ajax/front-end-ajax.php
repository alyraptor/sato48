<?php
/**
 * Plugin Name: Front-End Ajax
 * Plugin URI: http://www.sato48.com
 * Description: Ajax processing for the front-end
 * Version: 0.1.1
 * Author: Aly Richardson
 * Author URI: http://www.alyraptor.com
 * License: 
 */

//==============================
// Admin Pages
//==============================

function fea_admin_page() {

	global $fea_settings;
	$fea_settings = add_options_page(__('Front-End Ajax', 'fea'), __('Front-End Ajax', 'fea'), 'manage_options', 'front-end-ajax', 'fea_render_admin');

}
add_action('admin_menu', 'fea_admin_page');

function fea_render_admin() {
	?>
	<div class="wrap">
		<h2><?php _e('Front-End Ajax', 'fea'); ?></h2>
		<form id="fea-form" action="" method="POST">
			<div>
				<input type="submit" name="fea-submit" id="fea_submit" class="button-primary" value="<?php _e('Get Results', 'fea'); ?>"/>
				<img src="<?php echo admin_url(); ?>/images/wpspin_light.gif" class="waiting" id="fea_loading" style="display: none;">
			</div>
		</form>
		<div id="fea_results"></div>
	</div>
	<?php
}

function fea_load_scripts($hook) {

	global $fea_settings;

	if( $hook != $fea_settings )
		return;

	wp_enqueue_script('fea-ajax', plugin_dir_url(__FILE__) . 'js/fea-ajax.js', array('jquery'));
	wp_localize_script('fea-ajax', 'fea_vars', array(
			'fea_nonce' => wp_create_nonce('fea_post_connection', 'fea_nonce'),
			'ajaxurl' => admin_url('admin-ajax.php')
		)
	);

}
add_action('wp_enqueue_scripts', 'fea_load_scripts');

//==============================
// Connections
//==============================

function add_team_connection() {

	if ( isset( $_POST['fea_nonce'] ) && wp_verify_nonce( $_POST['fea_nonce'], 'fea_post_connection' ) ) {

		switch( $_POST['connection_type'] ) :
			case 'team_managers':

				$from = $_POST['user'];
				$to = $_POST['team'];
				$manager = true;

				break;
			case 'team_members':

				$from = $_POST['user'];
				$to = $_POST['team'];
				$manager = false;

				break;
		endswitch;

		if( !is_member($from, $to, $manager) ):
			$new_connection = new_post_connection( $from, $to );
		elseif( is_member($from, $to, $manager) ):
			$new_connection = 'duplicate';
			$user = get_user_by( 'id', $from );
			$user = $user->display_name;
			if( $manager == true ): $manager = 'manager'; else: $manager = 'member'; endif;
		else:
			$new_connection = false;
		endif;

		if( $new_connection == 'duplicate' ):
			echo '<div class="warning">' . $user . ' is already a ' . $manager . '.</div>';
		elseif( $new_connection == true ) :
			$connected_user = get_user_by( 'id', $from );
			echo '<li>' . $connected_user->display_name . ' <a class="delete-connection" href="#" data-user_id="' . $connected_user->ID . '" data-team_id="' . get_the_ID() . '" data-nonce="' . wp_create_nonce('fea_delete_connection_nonce') . '">Remove</a></li>';
		else :
			echo '<p>' . __('An error occurred.', 'fea') . '</p>';
		endif;

		// Add to this year's Members
		if( ($new_connection == true) && ($manager == false) ) :
				$team_post_id = $_POST['team'];
				$user_id = $_POST['user'];

				$years = get_field('years', $team_post_id);
				$year = array_keys($years);
				$year = end($year);
				$field_key = 'field_54f347590a3ad';

				$meta_key = 'years_' . $year . '_members';
				$meta_key2 = '_' . $meta_key;

				$field = get_field($meta_key, $team_post_id);
				$user = get_user_by('id', $user_id);
				$field[] = array(
					'user' => array(
						'ID' => $user->ID
					)
				);

				update_field($meta_key, $field, $team_post_id);
		endif;

		die();
	} else {
		echo 'your nonce sucks';
	}
}
add_action('wp_ajax_fea_post_connection', 'add_team_connection');


function delete_team_connection() {

	if ( isset( $_POST['fea_nonce'] ) && wp_verify_nonce( $_POST['fea_nonce'], 'fea_post_connection' ) ) {

		$connection_type = 'team_' . $_POST['type'];
		$p2p_id = get_connection( $connection_type, $_POST['user_id'], $_POST['team_id'] );

		if( p2p_delete_connection( $p2p_id ) ) :

			if( $connection_type == 'team_members' ):

				// Remove from this year's Members
				$team_post_id = $_POST['team_id'];
				$user_id = $_POST['user_id'];

				$years = get_field('years', $team_post_id);
				$years = array_keys($years);
				$year = end($years);

				$meta_key = 'years_' . $year . '_members';
				$field = get_field($meta_key, $team_post_id);

				foreach($field as $key => $user):
					if( $user['user']['ID'] == $user_id ):
						$pull_user = $key;
					endif;
				endforeach;

				if(isset($pull_user)):
					unset($field[$pull_user]);
				endif;
				$field = array_values($field);

				update_field($meta_key, $field, $team_post_id);

			endif;
			echo 'success';
		endif;

		die();
	} else {
		echo 'Your nonce sucks';
	}
}
add_action('wp_ajax_delete_connection', 'delete_team_connection');

//==============================
// Search
//==============================

function get_suggestions() {

	check_ajax_referer( 'fea_nonce', 'nonce', false );

	$term = '*' . $_REQUEST['term'] . '*';
	// Query for suggestions
	$users = get_users( array(
		'search' => $term,
		'search_columns' => array(
			'display_name',
			'user_email'
		)
	) );

	$team_id = $_REQUEST['team_id'];
	if($_REQUEST['role'] == 'add_member'):
		$role = 'member';
		$manager = false;
	elseif($_REQUEST['role'] == 'add_manager'):
		$role = 'manager';
		$manager = true;
	endif;

	// Initialise suggestions array
	$suggestions=array();

	foreach ($users as $user):
		$user_id = $user->ID;
		if( !(is_member($user_id, $team_id, $manager))):
			// Initialise suggestion array
			$suggestion = array();
			$suggestion['value'] = $user->display_name . ' (' . $user->user_email . ')';
			$suggestion['id'] = $user->ID;
			$suggestion['element'] = $manager ? 'add_manager' : 'add_member';
	 
			// Add suggestion to suggestions array
			$suggestions[]= $suggestion;
		endif;
	endforeach;
 
	// JSON encode and echo
	$response = $_GET["callback"] . "(" . json_encode($suggestions) . ")";
	echo $response;

	exit;
}
add_action( 'wp_ajax_search_autocomplete', 'get_suggestions' );

// Add ability to search display_name (to allow spaces in search)
function user_search_columns_bd($search_columns, $search, $this){
	if(!in_array('display_name', $search_columns)){
		$search_columns[] = 'display_name';
	}
	return $search_columns;
}
add_filter('user_search_columns', 'user_search_columns_bd' , 10, 3);