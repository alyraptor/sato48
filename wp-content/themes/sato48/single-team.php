<?php
$team_id = get_the_id();
$years = (array) get_field('years', $team_id);
$edit_post = get_permalink().'/manage';
$members = array();
if(!empty($years)):
	foreach($years as $year):
		$date = $year['date'];
		if(isset($year['film_connection'][0])):
			$connection = (array) $year['film_connection'][0];
			$films[$date] = array(
				'film_name' => $connection['post_title'],
				'film_id' => $connection['ID']
			);
		else: $films[$date] = array(
				'film_name' => 'No film connection'
			);
		endif;

		if(isset($year['members'])):
			$users = $year['members'];
			foreach($users as $user):
				$uname = $user['user']['user_nicename'];
				$members[$uname]['name'] = $user['user']['display_name'];
				if(isset($members[$uname]['years'])):
					$members[$uname]['years']++;
				else:
					$members[$uname]['years'] = 1;
				endif;
			endforeach;
		endif;
	endforeach;
krsort($films);
endif;
get_header(); ?>
<div class="container">
	<div class="content-wrap view-wrap">
		<h1>SATO48 Team</h1>
		<h2 style="font-family: Open Sans; margin: 0 2rem;"><?php echo get_field('team_name', $team_id); ?>

		<?php if( is_manager() ) { ?><p><a class="edit-post-link" href="<?php echo $edit_post; ?>">Edit Team Info</a></p><?php } ?></h2>

		<div class="team-films-table">
			<table>
				<tr>
					<th>Years</th>
					<th>
						Films
					</th>
				</tr>
				<?php
					if(!empty($films)):
						foreach($films as $year => $film):
				?>
				<tr>
					<td>
						<?php echo $year; ?>
					</td>
					<td>
						<?php if($film['film_name'] == 'No film connection'): echo '<p>' . $film['film_name'] . '</p>'; else: echo $film['film_name']; endif;//<a href="' . get_permalink($film['film_id']) . '/manage">' . $film['film_name'] . '</a> ?>
					</td>
				</tr>
				<?php
						endforeach;

						else: ?>
				<tr>
					<td>
						
					</td>
					<td>
						None
					</td>
				</tr>
					<?php endif;
				?>
			</table>
		</div>
		<div class="team-users-table">
			<table>
				<tr>
					<th># of Years</th>
					<th>
						User
					</th>
				</tr>
				<?php
					if(!empty($members)):
						foreach($members as $uname => $fields):
				?>
				<tr>
					<td>
						<?php echo $fields['years']; ?>
					</td>
					<td>
						<?php echo '<a href="' . home_url() . '/user/' .  $uname . '">' . $fields['name'] . '</a>'; ?>
					</td>
				</tr>
				<?php
						endforeach;
						else: ?>
				<tr>
					<td>
						
					</td>
					<td>
						None
					</td>
				</tr>		
				<?php endif; ?>
			</table>
		</div>
	</div>
</div>
<?php get_footer(); ?>