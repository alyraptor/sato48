<script>
  $(document).ready(function(){
    $('.carousel').carousel({
          interval: 4000
     });
  });
</script>



<!--  Carousel -->
<div id="home-carousel" class="carousel slide" data-ride="carousel"><!-- class of slide for animation -->
	
<!-- Indicators -->
<ol class="carousel-indicators">
	<li data-target="#home-carousel" data-slide-to="0" class="active"></li>
	<li data-target="#home-carousel" data-slide-to="1"></li>
	<li data-target="#home-carousel" data-slide-to="2"></li>
</ol>

	<div class="carousel-inner">
		<div class="item active"><!-- class of active since it's the first item -->
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel4.jpg" alt="" />
			<div class="carousel-caption">
				<p>Rikki Star in Fantasy, SATO48 2014</p>
			</div>
		</div>
		<div class="item">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel5.jpg" alt="" />
			<div class="carousel-caption">
				<p>The Event, SATO48 2014</p>
			</div>
		</div>
		<div class="item">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/carousel6.jpg" alt="" />
			<div class="carousel-caption">
				<p>Doors, SATO48 2014</p>
			</div>
		</div>
	</div><!-- END carousel-inner -->
	
  <a class="left carousel-control" href="#home-carousel" data-slide="prev">
    <i class="fa fa-chevron-left carousel-nav"></i>
  </a>
  <a class="right carousel-control" href="#home-carousel" data-slide="next">
    <i class="fa fa-chevron-right carousel-nav"></i>
  </a>
</div><!-- END carousel -->



