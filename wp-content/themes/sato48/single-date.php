<?php get_header(); ?>

<div class="content-wrap">
	<div class="breadcrumbs">
		<?php theme_breadcrumbs(); ?>
	</div>
</div>

<div class="single-wrap clear">

	<div class="event-large">

		<?php $small_date = DateTime::createFromFormat('l, F j, Y', get_field('start_date'));	?>

		<div class="date-inner">
			<div class="date-icon">
				<h3><?php echo $small_date->format('F'); ?></h3>
				<h1><?php echo $small_date->format('j'); ?></h1>
			</div>

			<?php if ( get_field( 'end_date' ) ) : ?>

			<?php $small_date_end = DateTime::createFromFormat('l, F j, Y', get_field('end_date')); ?>

			<h3>through</h3>
			<div class="date-icon">
				<h3><?php echo $small_date_end->format('F'); ?></h3>
				<h1><?php echo $small_date_end->format('j'); ?></h1>
			</div>

			<?php endif; ?>

		</div>
			
	</div>

	<div class="single-desc">

		<h1><?php the_field( 'event_title' ); ?></h1>

		<p><?php echo $small_date->format('l, F j, Y'); ?></p>

		<p><?php the_field( 'event_description' ); ?></p>

		<?php //<h3><a href="#">Add event to calendar</a></h3> ?>

	</div>

</div>

<?php if ( get_field( 'address' ) ) : ?>

<div class="single-wrap">

	<div class="location-desc">

		<h3>Location</h3>

		<p><?php the_field( 'location' ); ?></p>

		<p><?php the_field( 'address' ); ?></p>

		<p><?php the_field( 'city_state_zip' ); ?></p>

	</div>

	<div class="location-map">

		<?php 

		$full_address = get_field( 'address' ).' '.get_field( 'city_state_zip' ); 
		$full_address = str_replace(", ", ",", $full_address);
		$full_address = str_replace(" ", "+", $full_address);

		?>

			<iframe
			  width="500"
			  height="400"
			  frameborder="0" style="border:0"
			  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCrfG00HmqEJzcWp3KptJdwzSqgFD9Cp6w&q=<?php echo $full_address; ?>">
			</iframe>

	</div>
	
</div>

<?php endif; ?>

<?php get_footer(); ?>