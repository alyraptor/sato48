<?php 
/*
Template Name: Manage Team
*/

manage_post2( get_the_ID() );

$managers = p2p_type( 'team_managers' )->get_related( get_queried_object() );
$managers = $managers->query['connected_items'];
$members = p2p_type( 'team_members' )->get_related( get_queried_object() );
$members = $members->query['connected_items'];

global $wp_query;
if( isset( $wp_query->query_vars['team'] )) {
	$team_name = esc_attr($wp_query->query_vars['team']);
}
$team_id = get_the_ID();

get_header();
?>
<div class="form-wrap team-edit">
	<h3><span>Editing Team <?php echo the_field( 'team_name' ); ?></span></h3>
	<div class="edit-field">
		<form name="edit_team" method="post" action="">
		<label for="team_name">Team Name</label>
		<input type="text" id="team_name" value="<?php echo get_field( 'team_name' ); ?>" tabindex="1" size="20" name="team_name" disabled="disabled" />
			<input type="submit" value="Save" class="btn btn-submit" alt="Can't be edited before competition" title="Can't be edited before competition" disabled="disabled"></input>
			<input type="hidden" name="connection_type" value="team_members">
			<input type="hidden" name="action" value="manage_post" />
			<input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>">
			<?php // wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>
		</form>
	</div>

	<div class="team-members">
		<h5>Producers (Can Edit)</h5>
		<ul class="team-list" id="managers">

		<?php
			foreach( $managers as $manager ) {
				$user = get_user_by( 'id', $manager );
				echo '<li>'.$user->display_name.' <a class="delete-connection" href="#" data-user_id="' . $user->ID . '" data-team_id="' . get_the_ID() . '" data-fea_nonce="' . wp_create_nonce('fea_post_connection', 'fea_nonce') . '">Remove</a></li>';
			} 
		?>

		</ul>
		<div class="edit_field">
		<form id="fea-connection-manager" action="" method="POST">
			<label for="add_manager">Add Manager</label>
			<input type="text" id="add_manager" value="" tabindex="2" size="20" name="user_id" />
			<input type="submit" id="manager_submit" value="Add" name="add_manager" class="btn btn-submit" data-post_id="<?php echo $post->ID; ?>" disabled="disabled" />	
			<input type="hidden" name="connection_type" value="team_managers">
			<input type="hidden" name="action" value="new_post_connection" />
			<input type="hidden" name="team_id" id="team_id" value="<?php echo get_the_ID(); ?>">
			<input type="hidden" name="user_id" id="manager_id" value="">
			<?php wp_nonce_field( 'fea_post_connection', 'fea_nonce' ); ?>
		</form>
		</div>
	</div>


	<div class="team-members">
		<h5>Team Members </h5>
		<ul class="team-list" id="members">

		<?php
			foreach( $members as $member ) {
				$user = get_user_by( 'id', $member );
				echo '<li>'.$user->display_name.' <a class="delete-connection" href="#" data-user_id="' . $user->ID . '" data-team_id="' . get_the_ID() . '" data-fea_nonce="' . wp_create_nonce('fea_post_connection', 'fea_nonce') . '">Remove</a></li>';
			} 
		?>
	
		</ul>
		<div class="edit_field">
		<form id="fea-connection-member" action="" method="POST">
			<label for="add_member">Add Member</label>
			<input type="text" id="add_member" value="" tabindex="2" size="20" name="user_id" />
			<input type="submit" id="member_submit" value="Add" name="add_member" class="btn btn-submit" data-post_id="<?php echo $post->ID; ?>" disabled="disabled" />	
			<input type="hidden" name="connection_type" value="team_members">
			<input type="hidden" name="action" value="new_post_connection" />
			<input type="hidden" name="team_id" id="team_id" value="<?php echo get_the_ID(); ?>">
			<input type="hidden" name="user_id" id="member_id" value="">
			<?php wp_nonce_field( 'fea_post_connection', 'fea_nonce' ); ?>
		</form>
		</div>
	</div>

	<div class="form-footer">
		<a class="btn btn-neutral" href="<?php echo get_permalink($team_id); ?>">&laquo; Go back</a>
	</div>

<!-- 		<label for="add_member">Add Member</label>
		<input type="text" id="add_member" value="" tabindex="2" size="20" name="add_member" />

		<?php
			$nonce = wp_create_nonce("add_manager_nonce");
			$link = admin_url('admin-ajax.php?action=add_manager&post_id='.$post->ID.'&nonce='.$nonce);
			echo '<a style="display: inline-block; text-decoration: none; margin: 0 0 0 1.5rem;" class="user_vote btn btn-success" data-fea_nonce="' . $nonce . '" data-post_id="' . $post->ID . '" href="' . $link . '">Add</a>';
		?> -->
</div>

<?php get_footer(); ?>