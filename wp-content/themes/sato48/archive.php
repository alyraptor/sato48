<?php
	// Before you code, please read: http://codex.wordpress.org/Template_Hierarchy
	// Remember that any missing templates will redirect here, so use it wisely!
	get_header();
	the_post();
?>
<div class="container">
	<div class="content-wrap default">
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
	</div>
</div>
<?php get_footer(); ?>