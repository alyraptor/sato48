<?php 
//this is the alter ego of the Community page
?>

<?php get_header(); ?>

<div class="container" style="height: 54rem;">

	<div class="hero-image parallax" data-velocity=".3" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/crowd2.jpg');">

	</div>

</div>

<div class="social-container">

	<div class="social-box">

		<div class="sort-navigation">

			<div class="sort-categories">

				<h6><a href="#" id="sort-news">News</a> / <a href="#" id="sort-facebook">Facebook</a> / <a href="#" id="sort-twitter">Twitter</a> / <a href="#" id="sort-youtube">Youtube</a></h6>

			</div>

			<div class="feed-length">

				<h6><a href="#" id="sort-all">All</a> / <a href="#" id="sort-latest">Latest</a></h6>

			</div>

		</div>

		<div class="social-feed">

			<div class="feed-items">

				<?php 

					$args = array(
						array(
						    'service' => 'youtube',
					        'image' => 'http://localhost/sato48/wp-content/uploads/2014/12/filmmaking-mistakes.jpg',
					        'text' => 'How to Make Movies Like a Professional',
					        'timestamp' => '2014-11-26 12:03:20'
						),
						array(
						    'service' => 'facebook',
					        'image' => '',
					        'text' => 'And we have another episode of OUT OF DODGE from Brandon Vanderstine! We thank Brandon for sharing his work with us, for his kind words regarding SATO48, and to the SATO48 community for providing feedback. Don\'t miss OUT OF DODGE!',
					        'timestamp' => '2014-11-28 20:03:20'
						),
						array(
						    'service' => 'twitter',
					        'image' => '',
					        'text' => 'This Tweet has to be under 140 long! Better keep it brief.',
					        'timestamp' => '2014-11-29 20:07:20'
						),
						array(
						    'service' => 'facebook',
					        'image' => '',
					        'text' => 'Nic Stogsdill has kindly shared with us the final episode of his class project. Fellow filmmakers and film enthusiasts, when you get a chance, swing by this link and check it out. Feedback is encouraged!',
					        'timestamp' => '2014-11-30 19:03:20'
						),
						array(
						    'service' => 'news',
					        'image' => '',
					        'text' => 'Exciting news, everyone!  We\'ve recently decided the upcoming dates for SATO48 in 2015!  Stay tuned for more information as it comes out over the next 24 hours.',
					        'timestamp' => '2014-12-11 15:03:20'
						)
					);

					foreach ( $args as $item ) { // Display post-type class. If image, display, if none, display text
				?>

				<div class="feed-post <?php echo $item['service']; ?>-post"<?php if( isset( $item['image'] ) && $item['image'] != '') { 
						echo ' style="background-image: url(\'' . $item['image'] . '\')">&nbsp;';
					} else {
						echo "><h6>" . mb_strimwidth($item['text'], 0, 80, "...") . "</h6>";
					} ?>
					<div class="post-icon"></div>
					<a href="#"></a>
				</div>

				<?php } ?>

			</div>

			<div class="feed-footer">
				<h6><a href="#" id="load-more">Load More</a></h6>
			</div>

		</div>

	</div>

</div>

<?php get_footer(); ?>