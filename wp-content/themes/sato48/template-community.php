<?php
/*
Template Name: Community
*/
get_header();
the_post();?>

<div class="container" style="height: 60rem;">

	<div class="hero-image scroll" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/crowd2.jpg');">

	</div>

</div>
<div class="social-container">
	<div class="body-text content-wrap">
		<?php the_content(); ?>
		<?php //make sure to build these fields on the live version ?>
		<?php $fb_link = get_field('facebook_link');
			$tw_link = get_field('twitter_link');
			$hash_link = get_field('hashtag_link');
		?>
		<div class="news-img">
			<h2><a href="<?php get_bloginfo('url');?>/sato-in-the-news">SATO News</a></h2>
		</div>
		<div class="social-bars">
			<div class="social-bar">
				<h3><a href="<?php echo $fb_link; ?>" class="fb-link"><i class="fa fa-facebook"></i>Like Us on Facebook</a></h3>
			</div>
			<div class="social-bar">
				<h3><a href="<?php echo $tw_link; ?>" class="tw-link"><i class="fa fa-twitter"></i>Follow Us on Twitter</a></h3>
			</div>
			<div class="social-bar">
				<h3><a href="<?php echo $hash_link; ?>" class="hashtag"><i class="fa fa-slack"></i>Track Our Hashtag</a></h3>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>