<?php 
/*
Template Name: Contact
*/
get_header(); ?>

<div class="container" style="height: 60rem;">

	<div class="hero-image scroll" id="contact-scroll">

	</div>

</div>

<div class="container contact-container">

	<div class="content-wrap contact-wrap">

		<h2>Contact Us</h2>

		<p>Questions? Comments? Drop us a line!</p>

		<div class="form-wrap">

			<?php gravity_form( 1, $display_title = false, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false ); ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>