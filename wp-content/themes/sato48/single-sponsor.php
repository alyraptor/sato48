<?php get_header(); ?>

<div class="content-wrap">
	<div class="breadcrumbs">
		<?php theme_breadcrumbs(); ?>
	</div>
</div>

<div class="single-wrap clear">

	<div class="single-image">

		<img src="<?php the_field( 'sponsor_image' ); ?>" />

	</div>

	<div class="single-desc">

		<h1><?php the_field( 'sponsor_name' ); ?></h1>

		<p><?php the_field( 'sponsor_description' ); ?></p>

		<p>Years sponsored: <?php the_field( 'years' ); ?></p>

	</div>

</div>

<?php get_footer(); ?>