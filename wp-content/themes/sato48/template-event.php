<?php
/*
Template Name: Event
*/
get_header(); ?>

<div class="container" style="height: 60rem;">

	<div class="hero-image scroll" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/megaphone.jpg');">

	</div>

</div>

<?php

	$args = array(
		'post_type' => 'date',
		'meta_key' => 'start_date',
		'meta_type' => 'DATE',
		'orderby' => 'meta_value',
		'order' => 'ASC'
	);

	$the_query = new WP_Query( $args );

while ( $the_query->have_posts() ) : $the_query->the_post(); $title = get_the_title(); if( ($title == 'Kickoff Evening') ) : ?>

<div class="countdown-wrapper">
	<div class="clock" data-date="2015-4-10 07:00:00">
	</div>
	<div class="countdown-info">
		<h1>COUNTDOWN</h1>
		<h2>To <?php the_field( 'event_title' ); ?></h2>
		<h2><?php the_field( 'start_date' ); ?></h2>
		<h2><?php the_field( 'start_time' ); ?></h2>
		<div class="where">
			<h3><?php the_field( 'location' ); ?></h3>
			<h3><?php the_field( 'address' ); ?></h3>
			<h3><?php the_field( 'city_state_zip' ); ?></h3>
		</div>
		<h3><a href="<?php the_permalink(); ?>">Event Details</a></h3>
	</div>
</div>

<?php

		endif;
	endwhile;

	wp_reset_postdata();

	$args = array(
		'post_type' => 'date',
		'meta_key' => 'start_date',
		'meta_type' => 'DATE',
		'orderby' => 'meta_value',
		'order' => 'ASC'
	);

	$the_query = new WP_Query( $args );

if ( have_posts() ) : ?>


<div class="container">

	<div class="content-wrap">

		<div class="row date-row">

			<div class="date-titles">
				<h6>Important Dates</h6>
			</div>

			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<a class="date-link col-md-3" href="<?php the_permalink(); ?>">

					<div class="date-box">

						<h3><?php the_field( 'event_title' ); ?></h3>

						<?php $small_date = DateTime::createFromFormat('l, F j, Y', get_field('start_date')); ?>

						<div class="date-inner">
							<div class="date-icon">
								<h4><?php echo $small_date->format('M'); ?></h4>
								<h3><?php echo $small_date->format('j'); ?></h3>
							</div>

							<?php if ( get_field( 'end_date' ) ) : ?>

							<?php $small_date_end = DateTime::createFromFormat('l, F j, Y', get_field('end_date')); ?>

							<p></p>
							<div class="date-icon">
								<h4><?php echo $small_date_end->format('M'); ?></h4>
								<h3><?php echo $small_date_end->format('j'); ?></h3>
							</div>

						<?php endif; ?>

						</div>

					</div>

				</a>

			<?php endwhile; ?>

		</div>

	</div>

</div>

<?php endif; wp_reset_postdata(); ?>

<div class="container">

	<div class="content-wrap" id="event-bda">

		<div class="collapsible-header"><i class="fa fa-chevron-right"></i><h2><i class="fa fa-calendar"></i>Before</h2></div>

		<div class="collapsible">
			<?php the_field( 'before' ); ?>
		</div>

		<div class="collapsible-header"><i class="fa fa-chevron-right"></i><h2><i class="fa fa-video-camera"></i>During</h2></div>

		<div class="collapsible">
			<?php the_field( 'during' ); ?>
		</div>

		<div class="collapsible-header"><i class="fa fa-chevron-right"></i><h2><i class="fa fa-glass"></i>After</h2></div>

		<div class="collapsible">
			<?php the_field( 'after' ); ?>
		</div>

	</div>

</div>

<?php get_footer(); ?>