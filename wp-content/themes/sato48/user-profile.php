<?php 
/*
Template Name: User
*/
must_be_logged_in();
$user_current_teams = get_posts( array(
	'connected_type' => 'team_members',
	'connected_items' => $current_user,
	'suppress_filters' => false,
	'nopaging' => true
) );

foreach($user_current_teams as $team):
	$team_info = get_post_meta($team->ID);
	$current_teams[] = array(
		'name' => $team_info['team_name'][0],
		'id' => $team->ID
	);
endforeach;

$user_id = $current_user->ID;
$field_key = 'field_54a724d6abc6c';

$result = $wpdb->get_results($wpdb->prepare( 
	"
	SELECT *
	FROM wp_postmeta
	WHERE meta_key LIKE %s
		AND meta_value = %s
	",
	'years_%_members_%_user',
	$user_id
));

foreach($result as $row):
	$meta_rows[$row->post_id][] = intval(substr($row->meta_key, 6, 1));
endforeach;
if(!empty($meta_rows)):
	foreach($meta_rows as $team_id => $row):
		foreach($row as $year):
			$parent = get_field('years', $team_id);
			$date = $parent[$year]['date'];
			if(isset($parent[$year]['film_connection'][0]->ID)):
				$film_connection = (array) $parent[$year]['film_connection'][0];
			else:
				$film_connection = array( 'post_title' => '', 'ID' => '' );
			endif;
			$final_array[$date][] = array(
				'team_name' => $parent[$year]['team_name'],
				'team_id' => $team_id,
				'film_name' => $film_connection['post_title'],
				'film_id' => $film_connection['ID']
			);
		endforeach;
	endforeach;
krsort($final_array);
endif;

$message_color = get_field('general_message_priority', 'option');
$message = get_field('general_message', 'option');

if( get_user_role() == 'developer' ): $message = 'Testing'; $message_color = 'neutral'; endif;

get_header(); ?>
<div class="container">
	<div class="content-wrap view-wrap">
		<h1><?php the_title(); ?></h1>
		<div class="user-info">
			<div class="user-identity clear">

				<div class="user-info">
					<p>Hi, <?php echo $current_user->user_firstname; ?>!</p>
					<p>(Not you? <a href="<?php echo wp_logout_url(); ?>">Logout</a>)</p>
					<p><a href="<?php echo get_permalink().'user-information/';?>">View/Edit Information</a></p>
					<p><a href="<?php echo home_url() . '/user/' . $current_user->user_nicename;?>">View Profile</a></p>
				</div>
				<?php  if (!empty($message)) : ?>
				<div class="profile-message message-<?php echo $message_color; ?>">
					<p><?php echo $message; ?></p>
				</div>
				<?php endif; ?>
			</div>

			<div class="current-teams">
				<p>Current Team(s): <?php if(!empty($current_teams)): foreach($current_teams as $team): echo '<a href="' . get_permalink($team['id']) . '">' . $team['name'] . '</a>'; endforeach; else: echo "None"; endif; ?></p>
			</div>

			<?php if(!empty($final_array)): ?>

			<div class="user-teams-table">
				<table>
					<tr>
						<th></th>
						<th>
							My Films
						</th>
						<th>
							My Teams
						</th>
					</tr>
					<?php
						foreach($final_array as $year => $row):
							foreach($row as $info):
					?>
					<tr>
						<td>
							<?php echo $year; ?>
						</td>
						<td>
							<!--<a href="<?php //echo get_permalink($info['film_id']) . '/manage'; ?>">--><?php echo $info['film_name']; ?><!--</a>-->
						</td>
						<td>
							<a href="<?php echo get_permalink($info['team_id']); ?>"><?php echo $info['team_name']; ?></a>
						</td>
					</tr>
					<?php
							endforeach;
						endforeach;
					?>
				</table>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>