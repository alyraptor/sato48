<?php 
/*
Template Name: View Team
*/
?>

<?php get_header(); ?>

<div class="team-view">

<?php //print_r($post); ?>


	<?php //echo film_team_lookup(); ?>

	<?php //echo "ID: ".get_the_ID()."<br />"; ?>

	<?php $edit_post = get_permalink().'/manage'; ?>

	<?php the_field( 'team_name' ); ?>
	<?php if( is_manager() ) { ?><a href="<?php echo $edit_post; ?>" style="padding: 0 0 0 1rem;">Edit</a><br /><?php } ?>

	<?php //$owner = get_field('owner'); echo "Owner: ".$owner['display_name']."<br /><br />"; ?>

	<?php

	// check if the repeater field has rows of data
	if( have_rows('years') ) {

		echo "Participation Years<ul style=\"list-style: none;\">";

		$years = get_field('years');

		foreach( $years as $key => $row )
		{
			$column_id[ $key ] = $row['date'];
		}

		array_multisort( $column_id, SORT_DESC, $years );

		foreach( $years as $row ) {

			echo "<li>";

			echo $row['date'];
			echo ", ";
			echo $row['team_name'];
			echo ", ";
			echo $row['owner']['display_name'];
			if( $row['under_18'] ) { echo ", Under 18"; } else {};
			if( $row['film_connection'] ) {
				$film = $row['film_connection'];
				echo ", <a href=\"".get_permalink( $film[0]->ID )."\">".get_field( 'title', $film[0]->ID )."</a>";
			} else {}

			echo "</li>";

		}

		echo "</ul>";

	}

	?>

</div>

<?php get_footer(); ?>