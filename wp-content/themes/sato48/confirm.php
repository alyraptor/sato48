<?php
/*
Template Name: Confirm
*/

global $current_user;
get_currentuserinfo();
$user_id = $current_user->ID;

$args = array(
	'meta_key' => 'owner',
	'meta_value' => $user_id,
	'post_type' => 'team',
	'orderby' => 'post_date',
	'order' => 'DESC',
);
$posts = get_posts( $args );

$team_link = get_permalink( $posts[0]->ID);
$team_name = $posts[0]->post_title;
$cancelled = '';

if( !empty($_GET['method']) ):
	if( $_GET['method'] == 'Pay with cash (offline payment)' ):
		$method = 'cash';
	elseif( $_GET['method'] == 'cancel' ):
		$cancelled = '<h1>You cancelled your Paypal payment.</h1>';
	endif;
endif;

get_header();
?>

<div class="container">
	<div class="content-wrap">
		<div class="form-wrap">
			<div class="gform_confirmation_wrapper">
				<div class="gform_confirmation_message">
					Thank you for creating a team!
				</div>
			</div>

			<p><a href="<?php echo $team_link . '" style="font-weight: bold;">' . $team_name; ?></a> is ready to go. Please read the confirmation email that was just sent to you.</p>

			<?php if( !empty($method) && ($method == 'cash') ): ?>
				<?php echo $cancelled; ?><h3><span>Payment Instructions</span></h3><p>Please bring (or mail) $25.00 to 2470 S Wallis Smith Ave, Springfield, MO 65804 by Thursday, April 9, 2015 to be eligible for competition in SATO48 2015. If paying by check, <strong>be sure to include your TEAM NAME on your check,</strong> and make payable to Hlabeard Productions.</p>
			<?php endif; ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>