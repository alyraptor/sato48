<?php

// Remove default jQuery and add Google hosted version to the footer
// Are you building a site that doesn't require IE8?
// Consider using: //ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
function enqueue_custom_jquery() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'), false, '1.11.0', true);
    wp_enqueue_script('jquery');
}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_jquery', 0 );

// Add modernizr to the header
function enqueue_custom_modernizr() {
    wp_register_script('modernizr', (get_bloginfo('template_directory') . '/assets/js/vendor/modernizr.js'), false, '2.8.3', false);
    wp_enqueue_script('modernizr');
}
add_action( 'wp_enqueue_scripts', 'enqueue_custom_modernizr', 0 );

add_action( 'wp_enqueue_scripts', 'collapsible_enqueue' );
function collapsible_enqueue(){
    wp_register_script('collapsible', get_template_directory_uri().'/_src/js/plugins/collapsible.js', 'jquery', '', true);
    wp_enqueue_script('collapsible');
}

add_action( 'wp_enqueue_scripts', 'countdown_enqueue' );
function countdown_enqueue(){
    wp_register_script('countdown', get_template_directory_uri().'/assets/js/jquery-countdown-timer/inc/TimeCircles.js', 'jquery', '', true);
    wp_enqueue_script('countdown');
}

// add_action( 'wp_enqueue_scripts', 'parallax_enqueue' );
// function parallax_enqueue(){
//     wp_register_script('parallax', get_template_directory_uri().'/_src/js/plugins/parallax.js', 'jquery', '', true);
//     wp_enqueue_script('parallax');
// }

add_action( 'wp_enqueue_scripts', 'autocomplete_enqueue' );
function autocomplete_enqueue()
{
    wp_enqueue_script( 'jquery-ui-autocomplete' );
}

// Functions (Required)
include_once('functions/custom_post_types.php');
include_once('functions/custom_taxonomies.php');
include_once('functions/custom_sidebars.php');

include_once('functions/custom_post_connections.php');
include_once('functions/breadcrumbs.php');
include_once('functions/manage_post.php');
include_once('functions/check_connections.php');
include_once('functions/template_redirect.php');
include_once('functions/custom_rewrite_rules.php');
include_once('functions/gravity_forms.php');
include_once('functions/custom_shortcodes.php');
include_once('functions/css_function.php');
include_once('functions/user_functions.php');
include_once('functions/email_functions.php');

// Functions (Utility)
include_once('functions/wordpress/utility.php');

// Add CPT icons and seperators to the admin menu
include_once('functions/wordpress/admin-menu.php');

// Add Menu icon
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );

function your_custom_menu_item ( $items, $args ) {

    $items .= '<i class="fa fa-bars"></i>';
    
    return $items;
}


// Environment Management
include_once('functions/environment.php');

// Security
define('DISALLOW_FILE_EDIT', true);

// Remove admin bar
add_filter('show_admin_bar', '__return_false');

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

// CUSTOM OPTIONS MENUS
include_once('functions/options.php');

// SATO OPTIONS
include_once('functions/disable_comments.php');
include_once('functions/sato_functions.php');