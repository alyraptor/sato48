<?php 
/*
Template Name: User Information
*/
must_be_logged_in();
manage_user( get_current_user_id(), $_POST );
$countries = explode(", ", get_field('countries', 'option'));
array_unshift($countries, '');
get_header(); ?>
<div class="container">
	<div class="content-wrap view-wrap">
		<div class="user-info form-wrap">
			<h1><?php the_title(); ?></h1>
			<form class="user-form" name="edit_user" method="post" action="">
				<h4>User</h4>
				<div class="clear">
					<span class="input-left">
						<input type="text" name="first_name" value="<?php echo $current_user->first_name; ?>" tabindex="2">
						<label for="first_name">First Name</label>
					</span>
					<span class="input-right">
						<input type="text" name="last_name" value="<?php echo $current_user->last_name; ?>" tabindex="2">
						<label for="last_name">Last Name</label>
					</span>
				</div>
					<div class="clear">
					<span class="input-left">
						<input type="text" name="email" value="<?php echo $current_user->user_email; ?>" tabindex="4">
						<label for="email">Email</label>
					</span>
				</div>
				<h4>Address</h4>
				<div class="clear">
					<span class="input-full">
						<input type="text" name="address" value="<?php echo $current_user->address; ?>" tabindex="5">
						<label for="address">Street Address</label>
					</span>
					<span class="input-full">
						<input type="text" name="address2" value="<?php echo $current_user->address2; ?>" tabindex="6">
						<label for="address2">Address Line 2</label>
					</span>
					<span class="input-left">
						<input type="text" name="city" value="<?php echo $current_user->city; ?>" tabindex="7">
						<label for="city">City</label>
					</span>
					<span class="input-right">
						<input type="text" name="state" value="<?php echo $current_user->state; ?>" tabindex="8">
						<label for="state">State / Province / Region</label>
					</span>
					<span class="input-left">
						<input type="text" name="zip" value="<?php echo $current_user->zip; ?>" tabindex="9">
						<label for="zip">ZIP / Postal Code</label>
					</span>
					<span class="input-right">
						<select name="country" value="<?php echo $current_user->country; ?>" tabindex="10">
						<?php
							foreach($countries as $country):
								if($country == $current_user->country): $selected = 'selected'; else: $selected = ''; endif;
								echo '<option value="' . $country . '" ' . $selected . '>' . $country . '</option>';
							endforeach;
						?>
						</select>
						<label for="country">Country</label>
					</span>
				</div>
				<h4>Phone</h4>
				<div class="clear">
					<span class="input-left">
						<input type="text" name="phone" value="<?php echo $current_user->phone; ?>" tabindex="11">
						<label for="phone">Phone Number</label>
					</span>
				</div>
				<div class="buttons clear">
					<a class="btn btn-neutral" href="<?php echo get_home_url() . '/user-profile'; ?>">Back</a>
					<input type="submit" value="Save" class="btn btn-submit" tabindex="12">
					<input type="hidden" name="action" value="manage_user" />
					<?php wp_nonce_field( 'update_user_'. get_current_user_id(), 'update_user_nonce' ); ?>
				</div>
			</form>
		</div>
	</div>
</div>

<?php get_footer(); ?>