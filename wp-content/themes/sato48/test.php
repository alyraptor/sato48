<?php
/*
Template Name: Test
*/
get_header();

	/////// GRAVITY FORMS IPN 

	// global $wpdb;

	// $body = 'function started';

	// $entry_id = '159';
	// $field_key = 'field_54a724d6abc6c';

	// $result = $wpdb->get_results($wpdb->prepare( 
	// 	"
	// 	SELECT *
	// 	FROM wp_postmeta
	// 	WHERE meta_key LIKE %s
	// 		AND meta_value = %s
	// 	",
	// 	'years_%_gravity_id',
	// 	$entry_id
	// ));

	// $body .= "\n\$result: <pre>" . print_r($result, true) . "</pre>";

	// $team_post_id = $result[0]->post_id;

	// $repeater = get_field('years', $team_post_id);

	// foreach($repeater as $row):
	// 	if ($row['gravity_id'] == $entry_id):
	// 		$post_row = $row;
	// 	endif;
	// endforeach;

	// $body .= "\n\$post_row: <pre>" . print_r($post_row, true) . "</pre>";

	// $post_row['paid'] = 1;
	// $post_meta = array(
	// 	'years' => array(
	// 		'repeater',
	// 		'field_54a718a7e3340',
	// 		$post_row
	// 	)
	// );

	// $body .= "\n\$post_meta: <pre>" . print_r($post_meta, true) . "</pre>";

	// if( manage_post($team_post_id, 'team', $post_meta) ): $body .= "\nCompleted!"; endif;

	// $headers  = 'MIME-Version: 1.0' . "\r\n";
	// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	// $headers .= 'From: Aly <aly@40digits.com>' . "\r\n";

	// $to = array(
	// 	'aly.d.richardson@gmail.com'
	// );

	// foreach ($to as $person) {
	// 	$mailer = mail($person, '[Gravity ACF Test 1]', $body, $headers);

	// 	if ($mailer) {
	// 		echo $person . ' Message sent!<br />';
	// 	} else {
	// 		echo $person . ' Error in Email<br />';
	// 	}
	// }

	//////// INSERT FILMS CONNECTED TO TEAMS, MATCHING THEIR SLUGS AND CONNECTED
	// $args = array(
	// 	'post_type' => 'team',
	// 	'posts_per_page' => -1
	// );

	// $the_query = new WP_Query( $args );

	// if ( have_posts() ) :

	// 	while ( $the_query->have_posts() ) : $the_query->the_post();

	// 		$id = $post->id;
	// 		$team_number = $post->post_name;

	// 		$film_post_id = insert_post('film', $team_number, $team_number, false);

	// 		$repeater = get_field('years', $id);

	// 		foreach($repeater as $row):
	// 			$post_row = $row;
	// 		endforeach;

	// 		$post_row['film_connection'] = $film_post_id;
	// 		$post_meta = array(
	// 			'years' => array(
	// 				'repeater',
	// 				'field_54a718a7e3340',
	// 				$post_row
	// 			)
	// 		);
	// 		manage_post($id, 'team', $post_meta);
	// 		$new_connection = new_post_connection( $id, $film_post_id, 'team_to_film', false );

	// 		echo "<p>Team " . $team_number . " success!</p>";

	// 	endwhile;
	// endif;



	//////// DELETE REPEATER FIELD USING ACF UPDATE_FIELD
	// $team_post_id = '481';
	// $user_id = 4;

	// $years = get_field('years', $team_post_id);
	// $years = array_keys($years);
	// $year = end($years);

	// $meta_key = 'years_' . $year . '_members';

	// $field = get_field($meta_key, $team_post_id);

	// // print_r($field);

	// foreach($field as $key => $user):
	// 	if( $user['user']['ID'] == $user_id ):
	// 		$pull_user = $key;
	// 	endif;
	// endforeach;

	// if(isset($pull_user)):
	// 	unset($field[$pull_user]);
	// endif;
	// $field = array_values($field);

	// print_r($field);

	// update_field($meta_key, $field, $team_post_id);


	//////// ADD REPEATER FIELD USING ACF UPDATE_FIELD
	// $team_post_id = '480';
	// $user_id = get_current_user_id();

	// $years = get_field('years', $team_post_id);
	// $year = array_keys($years);
	// $year = end($year);

	// $field_key = 'field_54f347590a3ad';

	// $meta_key = 'years_' . $year . '_members';
	// $meta_key2 = '_' . $meta_key;

	// $user = get_user_by('id', $user_id);
	// $field[] = array(
	// 	'user' => array(
	// 		'ID' => $user->ID
	// 	)
	// );

	// update_field($meta_key, $field, $team_post_id);


	///////// DELETE POST META W/O USING ACF UPDATE_FIELD
	// 	$team_post_id = '473';
	// $user_id = get_current_user_id();

	// $years = get_field('years', $team_post_id);
	// $years = array_keys($years);
	// $year = end($years);

	// $date_query = get_field('site_year', 'option') . '0101';

	// $meta_key = 'years_' . $year . '_members_%s';// . $member . '_user';

	// $field_key = 'field_54f347590a3ad';

	// $result = $wpdb->get_results($wpdb->prepare( 
	// 	"
	// 	SELECT *
	// 	FROM wp_postmeta
	// 	WHERE meta_key LIKE %s
	// 		AND meta_value = %s
	// 		AND post_id = %d
	// 	",
	// 	'years_' . $year .'_members_%_user',
	// 	$user_id,
	// 	$team_post_id
	// ));

	// $member = array_keys($result);
	// $member = $member[0];

	// $meta_key = 'years_' . $year . '_members_%s' . $member . '_user';
	// $meta_key2 = '_' . $meta_key;

	// delete_post_meta($team_post_id, $meta_key, $user_id);
	// delete_post_meta($team_post_id, $meta_key2, $field_key);

get_footer();
?>