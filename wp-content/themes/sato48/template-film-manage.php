<?php 
/*
Template Name: Manage Film
*/

manage_post2( get_the_ID() );

global $wp_query;
if( !empty( $wp_query->query_vars['film'] )) {
	$film_id = esc_attr($wp_query->query_vars['film']);
}
$team_id = get_the_ID();

get_header();
?>
<div class="form-wrap film-edit">
	<h3><span>Editing Film <?php echo the_field( 'film_name' ); ?></span></h3>
	<form class="edit-film" name="film_info" id="edit_film" method="post" action="">
		<div class="edit-field">
			<label for="film_name">Film Name</label>
			<input type="text" id="film_name" value="<?php echo get_field( 'film_name' ); ?>" tabindex="1" size="20" name="film_name" />
		</div>
		<div class="edit-field">
			<label for="film_description">Film Description</label>
			<textarea id="film_description" tabindex="2" rows="4" name="film_description" form="edit_film"><?php echo get_field( 'film_description' ); ?></textarea>
		</div>
		<input type="submit" value="Save" class="btn btn-submit"></input>
		<input type="hidden" name="action" value="manage_post" />
		<input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>">
		<?php wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>
	</form>

		<h5>Cast and Crew</h5>
		<div class="team-users-table">
			<table>
				<tr>
					<th></th>
					<th>
						Cast/Crew Member
					</th>
					<th></th>
					<th></th>
				</tr>
				<?php
				if( have_rows( 'cast_crew' ) ) :
					while( have_rows ( 'cast_crew' ) ) : the_row();
						$member = get_sub_field('first_name') . ' ' . get_sub_field('last_name');
						$crew_id = get_sub_field('crew_id');
						if( get_sub_field('user') ):
							$user = get_sub_field('user');
							$user_link = '<a href="' . home_url() . '/user/' . $user['user_nicename'] . '">' . $user['display_name'] . '</a>';
						endif;
				?>
				<tr>
					<td></td>
					<td>
						<?php if( !empty($user_link)  ): echo $user_link; else: echo $member; endif; ?>
					</td>
					<td>
					</td>
					<td>
						<!-- <form class="remove-crew" name="remove_crew" id="remove_crew" method="post" action="">
							<input type="submit" value="Remove"></input>
							<input type="hidden" name="action" value="remove_crew" />
							<input type="hidden" name="crew_id" value="<?php echo $crew_id; ?>" />
							<?php wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>
						</form> -->
					</td>
				</tr>
				<?php
						unset($user_link);
					endwhile;
					else: ?>
				<tr>
					<td>
						
					</td>
					<td>
						None
					</td>
				</tr>		
				<?php endif; ?>
			</table>
		</div>
		<?php global $validation; if(!empty($validation)): echo '<p style="color: red;">Please fill out all fields to add a team member.</p>'; endif; ?>
	<form class="edit-crew" name="edit_crew" id="edit_crew" method="post" action="">
		<div class="edit-field">
			<label for="email">Email Address</label>
			<input type="text" id="email" value="<?php echo get_field( 'email' ); ?>" tabindex="4" size="20" name="email" />
		</div>
		<div class="edit-field">
			<label for="first_name">First Name</label>
			<input type="text" id="first_name" value="<?php echo get_field( 'first_name' ); ?>" tabindex="5" size="20" name="first_name" />
		</div>
		<div class="edit-field">
			<label for="last_name">Last Name</label>
			<input type="text" id="last_name" value="<?php echo get_field( 'last_name' ); ?>" tabindex="5" size="20" name="last_name" />
		</div>
		<div class="edit-field">
			<label for="address">Address</label>
			<input type="text" id="address" value="<?php echo get_field( 'address' ); ?>" tabindex="5" size="20" name="address" />
		</div>
		<div class="edit-field">
			<label for="address2">Address Line 2</label>
			<input type="text" id="address2" value="<?php echo get_field( 'address2' ); ?>" tabindex="5" size="20" name="address2" />
		</div>
		<div class="edit-field">
			<label for="city">City</label>
			<input type="text" id="city" value="<?php echo get_field( 'city' ); ?>" tabindex="5" size="20" name="city" />
		</div>
		<div class="edit-field">
			<label for="state">State</label>
			<input type="text" id="state" value="<?php echo get_field( 'state' ); ?>" tabindex="5" size="20" name="state" />
		</div>
		<div class="edit-field">
			<label for="zip">ZIP Code</label>
			<input type="text" id="zip" value="<?php echo get_field( 'zip' ); ?>" tabindex="5" size="20" name="zip" />
		</div>
		<div class="edit-field">
			<label for="phone">Phone Number</label>
			<input type="text" id="phone" value="<?php echo get_field( 'phone' ); ?>" tabindex="5" size="20" name="phone" />
		</div>
		<input type="submit" value="Add Cast/Crew Member" class="btn btn-submit"></input>
		<input type="hidden" name="crew_id" value="<?php if(!empty($crew_id) ): $crew_id++; echo $crew_id; else: echo '1'; endif; ?>" />
		<input type="hidden" name="action" value="manage_crew" />
		<input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>">
		<?php wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>
	</form>

	<form class="edit-nominations" name="edit_nominations" id="edit_nominations" method="post" action="">
		<h5>Nominations</h5>
		<?php
			if( have_rows( 'cast_crew' ) ) :
				while( have_rows ( 'cast_crew' ) ) : the_row();
					$member = get_sub_field('first_name') . ' ' . get_sub_field('last_name');
					$options[] = array(get_sub_field('crew_id'), $member);
					$roles = explode(",", get_field('nominations_list', 'option'));
				endwhile;
				array_unshift($options,'');
		?>
		<div class="edit-field">
			<?php
				$fields = get_field('nominations', get_the_ID());
				// print_r($fields);
				foreach($roles as $role_space):
					$role = str_replace(' ', '_', $role_space);
					echo '<label for="' . $role . '">' . $role_space . '</label>';
					echo '<select name="' . $role . '">';
					foreach($options as $option):
						if($fields[$role] == $option[0]): $selected = 'selected'; else: $selected = ''; endif;
						echo '<option value="' . $option[0] . '" ' . $selected . '>' . $option[1] . '</option>';
					endforeach;
					echo '</select>';
				endforeach;
			?>
		<input type="submit" value="Save Nominations" name="save_nominations" class="btn btn-submit"></input>
		<input type="hidden" name="action" value="manage_nominations" />
		<input type="hidden" name="user_id" value="<?php echo get_current_user_id(); ?>">
		<?php wp_nonce_field( 'update_post_'. get_the_ID(), 'update_post_nonce' ); ?>
		</form>
		</div>
		<?php else: ?>
		(Add crew members first)
		<?php endif; ?>
	</form>
</div>

<?php get_footer(); ?>