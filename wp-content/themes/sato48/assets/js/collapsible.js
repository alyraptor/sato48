jQuery(document).ready(function($) {
	$(".collapsible-header").click(function () {

	    $header = $(this);
	    //getting the next element
	    $content = $header.next();
	    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
	    $header.toggleClass("active");
	    $content.slideToggle(300, function () {
	        //execute this after slideToggle is done
	    });

	});

});