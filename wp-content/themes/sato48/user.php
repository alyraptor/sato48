<?php

$user = get_user_by( 'slug', $wp_query->query['author_name'] );

$user_current_teams = get_posts( array(
	'connected_type' => 'team_members',
	'connected_items' => $user,
	'suppress_filters' => false,
	'nopaging' => true
) );

foreach($user_current_teams as $team):
	$team_info = get_post_meta($team->ID);
	$current_teams[] = array(
		'name' => $team_info['team_name'][0],
		'id' => $team->ID
	);
endforeach;

$user_id = $user->ID;
$field_key = 'field_54a724d6abc6c';

$result = $wpdb->get_results($wpdb->prepare( 
	"
	SELECT *
	FROM wp_postmeta
	WHERE meta_key LIKE %s
		AND meta_value = %s
	",
	'years_%_members_%_user',
	$user_id
));

foreach($result as $row):
	$meta_rows[$row->post_id][] = intval(substr($row->meta_key, 6, 1));
endforeach;
foreach($meta_rows as $team_id => $row):
	foreach($row as $year):
		$parent = get_field('years', $team_id);
		$date = $parent[$year]['date'];
		if(isset($parent[$year]['film_connection'][0]->ID)):
			$film_connection = (array) $parent[$year]['film_connection'][0];
		else:
			$film_connection = array( 'post_title' => '', 'ID' => '' );
		endif;
		$final_array[$date][] = array(
			'team_name' => $parent[$year]['team_name'],
			'team_id' => $team_id,
			'film_name' => $film_connection['post_title'],
			'film_id' => $film_connection['ID']
		);
	endforeach;
endforeach;
krsort($final_array);

get_header(); ?>
<div class="container">
	<div class="content-wrap view-wrap">
		<h1>SATO48 Filmmaker</h1>
		<span class="filmmaker-name"><?php echo $user->display_name; ?></span>
		<div class="user-info">

			<div class="user-teams-table">
				<table>
					<tr>
						<th>Years</th>
						<th>
							Films
						</th>
						<th>
							Teams
						</th>
					</tr>
					<?php
						if(!empty($final_array)):
							foreach($final_array as $year => $row):
								foreach($row as $info):
					?>
					<tr>
						<td>
							<?php echo $year; ?>
						</td>
						<td>
							<a href="<?php echo get_permalink($info['film_id']); ?>"><?php echo $info['film_name']; ?></a>
						</td>
						<td>
							<a href="<?php echo get_permalink($info['team_id']); ?>"><?php echo $info['team_name']; ?></a>
						</td>
					</tr>
					<?php
								endforeach;
							endforeach;
							else: ?>
					<tr>
						<td>
							<?php echo $year; ?>
						</td>
						<td>
							None
						</td>
						<td>
							None
						</td>
					</tr>
					<?php
						endif;
					?>
				</table>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>