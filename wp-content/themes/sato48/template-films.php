<?php 
/*
Template Name: Films
*/
get_header(); ?>

<div class="container" style="height: 60rem;">

	<div class="hero-image scroll" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/camera.jpg');">

	</div>

</div>

<div class="container film-container">

	<div class="content-wrap film-splash">

		<h2>2015 Films</h2>

		<p>Films will be viewable here after the 2015 competition has finished.</p>

	</div>

</div>

<?php get_footer(); ?>