jQuery(document).ready(function($) {
	$(".collapsible-header").click(function () {

		$header = $(this);
		//getting the next element
		$content = $header.next();
		//open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
		$header.toggleClass("active");
		$content.slideToggle(300, function () {
			//execute this after slideToggle is done
		});

	});

	// Main Menu Dropdown

	$("#menu-main-nav > li").click(function (event) {
		if ($(window).width() < 960){
			event.stopPropagation();

			var menu = $("#menu-main-nav");
			if( menu.hasClass("nav-down") ) {

			} else {
				event.preventDefault();
				menu.toggleClass( "nav-down" );
			}

		}

	});

	$("#menu-main-nav > i").click(function () { // Remove class if user clicks icon
		if ($(window).width() < 960){

			var menu = $("#menu-main-nav");
			menu.toggleClass( "nav-down" );

		}

	});

	$('html').click(function() { // Or if user clicks outside menu
		var menu = $("#menu-main-nav");
		if( menu.hasClass("nav-down") ) {
			menu.toggleClass( "nav-down" );
		}
	});

	$(document).keyup(function(e) { // Or if escape is clicked
		var menu = $("#menu-main-nav");
		if(e.keyCode == 27) {
			if( menu.hasClass("nav-down") ) {
				menu.toggleClass( "nav-down" );
			} 
			if( $("#user-menu").hasClass( "active" ) ) {
				$header = $("#user-menu");
				//getting the next element
				$content = $header.next();
				//open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
				$header.toggleClass("active");
				$content.slideToggle(300, function () {

				});
			}
		}
	});

});