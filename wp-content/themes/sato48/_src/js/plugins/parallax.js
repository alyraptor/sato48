$( document ).ready(function() {

	// PARALLAX //////////////////////////////////////////////////
	
	$('.hero-image','body').each(function(){
		var obj = $(this);
	
		$(window).scroll(function() {
			var scrollTop     = $(window).scrollTop(),
			elementOffset = $(obj).offset().top,
			distance      = (elementOffset - scrollTop);
			if(distance <= $(window).height() && $(obj).attr('id') == 'intro'){
				 var yPos = (distance / 3 ); 
				 var bgpos = '50% '+ yPos + 'px'; 
				 $(obj).css('background-position', bgpos ); 
			}else{
				var yPos = (distance / -3 ); 
				var bgpos = '50% '+ yPos + 'px'; 
				$(obj).css('background-position', bgpos ); 
			}
		}); 
	});
});