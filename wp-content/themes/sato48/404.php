<?php
/**
 * 404 Page
 *
 */
get_header(); ?>
	<div class="container">
		<div class="content-wrap not-found">
			<h1>Take five, everybody!</h1>
			<p>Looks like we can't find the page you're looking for.</p>
			<h3 class="return"><a href="<?php echo get_bloginfo('url');?>"><i class="fa fa-arrow-left"></i>Return Home</a></h3>
		</div>
	</div>
</div>
<?php get_footer(); ?>