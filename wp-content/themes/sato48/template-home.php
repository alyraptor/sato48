<?php 
/*
Template Name: Home
*/

$current_year = get_field('site_year', 'option');
get_header(); ?>

<div class="container" style="height: 60rem;">

	<div class="hero-image scroll" id="home-scroll" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/trophies.jpg');">

		<div class="hero-inner"><p>48 Hours to Make a 5-Minute Film</p></div>

	</div>

</div>

<?php

	$args = array(
		'post_type' => 'featurebox',
		'posts_per_page' => '3',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$the_query = new WP_Query( $args );
?>

<?php if ( have_posts() ) : ?>

	<div class="container">

		<div class="row box-row">

		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	
			<div class="feature-box" style="background-image: url('<?php the_field( 'image' ); ?>'); ";>

				<div class="sato-<?php the_field( 'overlay_color' ); ?>"></div>

				<div class="feature-inner">
						<h1><span class="fa-icon"><?php the_field( 'icon' ); ?></span><?php the_field( 'header' ); ?></h1>
						<h3><?php the_field( 'subheader' ); ?></h3>
				</div>

				<a href="<?php the_field( 'url' ); ?>"></a>

			</div>

		<?php endwhile; ?>

		</div>

	</div>

<?php  else: endif; ?>

<?php wp_reset_postdata(); ?>

<?php

	$args = array(
		'post_type' => 'sponsor',
		'meta_query' => array(
			array(
				'key' => 'years',
				'value' => $current_year,
				'compare' => 'LIKE'
			)
		),
		'posts_per_page' => '8',
		'orderby' => 'rand'
	);

	$the_query = new WP_Query( $args );

?>

<div class="container">

	<div class="row spon-row">

		<div class="content-wrap spon-titles">
			<h6 style="display: inline;"><?php echo $current_year; ?> Sponsors</h6>
			<p><a href="<?php echo home_url(); ?>/sponsor">View all sponsors</a></p>
		</div>

		<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	
			<div class="col-one-four spon-box">

				<div class="spon-inner" style="background-image: url('<?php the_field( 'sponsor_image' ); ?>');"></div>

				<div class="sato-gray"></div>

				<h2><?php the_field( 'sponsor_name' ); ?></h2>

				<a href="<?php the_permalink(); ?>"></a>
			
					
			</div>

		<?php endwhile; else: ?>

		<span class="no-sponsors"><p>No sponsors to show at this time.</p></span>

		<?php endif; wp_reset_postdata();?>

	</div>

</div>

<div class="band-about">

	<div class="about-circle">

		<div class="about-circle-shape">

			<div id="about-image" style="background-image: url('<?php bloginfo('template_directory') ?>/assets/images/jeffkyaw.jpg');">

				<div class="circle-overlay"><h1 style="color: white;">About SATO48</h1></div>

			</div>
		
		</div>

	</div>

	<div class="blurb-wrap <?php if( get_field('center_blurb') ): echo 'blurb-wrap-center'; endif; ?>">

		<div class="about-blurb">

			<?php echo get_field('about_blurb'); ?>

		</div>

	</div>

</div>

<?php get_footer(); ?>