<!DOCTYPE html>
<!--[if lte IE 9]><html class="no-js lt-ie10"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="theme-color" content="#ee7421">

	<title><?php if ( is_home() ) { bloginfo('name'); } else { wp_title('&raquo;','true','right'); bloginfo('name'); } ?></title>

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/style.css" />
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />

	<link rel="shortcut icon" type="image/ico" href="<?php echo get_bloginfo('template_directory'); ?>/favicon.ico" />
	<link rel="apple-touch-icon" href="<?php echo get_bloginfo('template_directory'); ?>/assets/images/favicon/touch-60.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_bloginfo('template_directory'); ?>/assets/images/favicon/touch-76.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_bloginfo('template_directory'); ?>/assets/images/favicon/touch-120.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_bloginfo('template_directory'); ?>/assets/images/favicon/touch-152.png">

	<link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/font-awesome/css/font-awesome.min.css">

	<script>
		// Place Google Analytics code here

		// Set up site configuration
		window.config = window.config || {};

		// The base URL for the WordPress theme
		window.config.baseUrl = "<?php echo get_bloginfo('url'); ?>";

		// Empty default Gravity Forms spinner function
		var gformInitSpinner = function() {};
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>  id="<?php echo get_template_name(); ?>">
	<div class="page-wrap">

		<nav class="navbar container" role="navigation">
			
			<div class="content-wrap">

					<?php 

						$args = array(
							'menu' => 'main-menu',
							'echo' => true,
							'container'       => '',
							'container_class' => '',
							'menu_class'      => '',
							'items_wrap'      => '<ul id="%1$s" class="nav-list %2$s">%3$s</ul>',
						);

						echo strip_tags(wp_nav_menu( $args ), '<a>, <ul>');
					?>
				<?php
				//<form class="navbar-form navbar-left" role="search">
					//<div class="form-group">
					  //<input type="text" class="form-control" placeholder="Search...">
					//</div>
					//<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
				//</form>
				?>  	

						<ul class="navbar-right">
						
						<?php if( is_user_logged_in() ) { 
							global $current_user;
						?>
						<li class="user-navigation dropdown logged-in">
							<a href="#" class="collapsible-header" data-toggle="dropdown"><button class="btn btn-usernav"><?php echo $current_user->display_name; ?> <i class="fa fa-caret-down"></i></button></a>
							<ul class="collapsible">
								<?php if( ( get_user_role() == 'administrator' ) || ( get_user_role() == 'developer' ) ) { ?>
								<li><a href="<?php echo home_url(); ?>/wp-admin">Admin Dash</a></li><?php } ?>
								<li><a href="<?php echo home_url(); ?>/user-profile">My Profile</a></li>
								<li><a href="https://fs9.formsite.com/thisthis/form41/form_login.html">Team.Film.Nominations</a></li>
								<li><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
							<?php } else { ?>
						<li class="user-navigation"><a href="<?php echo home_url(); ?>/login/"><button class="btn btn-usernav">Login</button></a></li>
						<li class="user-navigation"><a href="<?php echo home_url(); ?>/register/"><button class="btn btn-usernav">Register</button></a></li>
								
							<?php } ?>
							</ul>

						</li>

					</ul>

			</div><!-- END Container -->
		</nav><!-- END navigation -->