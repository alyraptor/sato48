<?php

// Get an array of years from range
function years_array($start, $finish) {

	$years = array(); // An array to hold all years between 2004 and present
	$add_year = $start; // A counter for $years
	if ( $finish = 'present' ) { // Allow $end_year to be dynamic
		$end_year = date("Y");
	} else {
		$end_year = $finish;
	}

	while ( $add_year <= $end_year ) { // Giving $years its proper values

		$years[] = $add_year;
		$add_year++;

	}

	$years = array_reverse( $years , true );

	return $years;

}

// Return role of current user
function get_user_role() {
	global $wp_roles;

	foreach ( $wp_roles->role_names as $role => $name ) :

		if ( current_user_can( $role ) )
			$user_role = $role;

	endforeach;

return $user_role;
}

// function patricks_billing_fields( $fields ) {
// 	global $woocommerce;

//   // we don't need the billing fields so empty all of them except the email
//   unset( $fields['billing_country'] );
//   unset( $fields['billing_first_name'] );
//   unset( $fields['billing_last_name'] );
//   unset( $fields['billing_company'] );
//   unset( $fields['billing_address_1'] );
//   unset( $fields['billing_address_2'] );
//   unset( $fields['billing_city'] );
//   unset( $fields['billing_state'] );
//   unset( $fields['billing_postcode'] );
//   unset( $fields['billing_phone'] );

// 	return $fields;
// }
// add_filter( 'woocommerce_billing_fields', 'patricks_billing_fields', 20 );

///////////////////////////////////////////

// /* Remove Woocommerce User Fields */
// add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
// add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );
// add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );

// function custom_override_checkout_fields( $fields ) {
//   unset($fields['billing']['billing_state']);
//   unset($fields['billing']['billing_country']);
//   unset($fields['billing']['billing_company']);
//   unset($fields['billing']['billing_address_1']);
//   unset($fields['billing']['billing_address_2']);
//   unset($fields['billing']['billing_postcode']);
//   unset($fields['billing']['billing_city']);
//   unset($fields['shipping']['shipping_state']);
//   unset($fields['shipping']['shipping_country']);
//   unset($fields['shipping']['shipping_company']);
//   unset($fields['shipping']['shipping_address_1']);
//   unset($fields['shipping']['shipping_address_2']);
//   unset($fields['shipping']['shipping_postcode']);
//   unset($fields['shipping']['shipping_city']);
//   return $fields;
// }
// function custom_override_billing_fields( $fields ) {
//   unset($fields['billing_state']);
//   unset($fields['billing_country']);
//   unset($fields['billing_company']);
//   unset($fields['billing_address_1']);
//   unset($fields['billing_address_2']);
//   unset($fields['billing_postcode']);
//   unset($fields['billing_city']);
//   return $fields;
// }
// function custom_override_shipping_fields( $fields ) {
//   unset($fields['shipping_state']);
//   unset($fields['shipping_country']);
//   unset($fields['shipping_company']);
//   unset($fields['shipping_address_1']);
//   unset($fields['shipping_address_2']);
//   unset($fields['shipping_postcode']);
//   unset($fields['shipping_city']);
//   return $fields;
// }
// /* End - Remove Woocommerce User Fields */

// /* Make Woocommerce Phone Field Not Required  */
// add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1 );
// function wc_npr_filter_phone( $address_fields ) {
// 	$address_fields['billing_phone']['required'] = false;
// 	return $address_fields;
// }
// /* End - Make Woocommerce Phone Field Not Required  */