<?php 

function is_member( $user = null, $team = null, $manager = false ) {

	if( is_user_logged_in() ) {

		if( $user == null ) : $user = get_current_user_id(); endif;
		if( $team == null ) : $team = get_the_id(); endif;
		if( $manager == true ) : $connection = 'team_managers'; else: $connection = 'team_members'; endif;

		if( p2p_connection_exists( $connection, array( 'from' => $user, 'to' => $team ) ) ) {

			return true;

		} else {

			return false;

		}

	}

}

function is_manager( $user = null, $team = null ) {

	if( $user == null ) : $user = get_current_user_id(); endif;
	if( $team == null ) : $team = get_the_id(); endif;

	if( p2p_connection_exists( 'team_managers', array( 'from' => $user, 'to' => $team ) ) ) {
		return true;
	} else {
		return false;
	}
}

function team_made_film( $team = null, $film = null ) {

	if( $film == null ) : $film = get_the_id(); endif;
	if( $team == null ) : $team = film_team_lookup( $film ); endif;

	if( p2p_connection_exists( 'team_to_film', array( 'from' => $team, 'to' => $film ) ) ) {

		return true;

	} else {

		return false;

	}

}

function user_made_film( $user = null, $film = null ) {

	if( $user == null ) : $user = get_current_user_id(); endif;
	if( $film == null ) : $film = get_the_id(); endif;

	$team = film_team_lookup( $film );

	if( is_manager( $user, $team ) ) {

		return true;		

	} else {

		return false;

	}

}

function film_team_lookup( $film = null ) {

	if( $film == null ) : $film = get_the_id(); endif;

	$film = get_post( $film );

	$team = p2p_type( 'team_to_film' )->get_related( $film );

	return $team->query['connected_items'][0];
	 
}


function get_user_from_connection( $connection, $direction = 'from' ) { // Does not work as-is

	$key = ( 'from' == $direction ) ? 'p2p_from' : 'p2p_to';

	$new_connection = p2p_get_connection( $connection );

	$user = get_user_by( 'id', $new_connection->$key );

	if( $user == false ) { echo 'boobs'; }
	
	return $user;
}

function get_connection( $connection_type, $a, $b ) { // Does not work as-is

	$p2p_id = p2p_type( $connection_type )->get_p2p_id( $a, $b );
	
	return $p2p_id;
}