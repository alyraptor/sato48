<?php
// http://codex.wordpress.org/Function_Reference/register_post_type

function register_CPTs()
{

	/*

	// Duplicate this for each CPT.

	$labels = array(
		'name' => _x('Portfolio', 'post type general name'),
		'singular_name' => _x('Portfolio Item', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio'),
		'add_new_item' => __('Add New Portfolio Item'),
		'edit_item' => __('Edit Portfolio Item'),
		'new_item' => __('New Portfolio Item'),
		'view_item' => __('View Portfolio Item'),
		'search_items' => __('Search Portfolio'),
		'not_found' =>  __('No Portfolio Items found'),
		'not_found_in_trash' => __('No Portfolio Items found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Portfolio'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'project'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 21,
		'supports' => array('title','page-attributes')
	);

	register_post_type('portfolio',$args);


	*/

	$labels = array(
		'name' => _x('Feature Boxes', 'post type general name'),
		'singular_name' => _x('Feature Box', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio'),
		'add_new_item' => __('Add New Feature Box'),
		'edit_item' => __('Edit Feature Box'),
		'new_item' => __('New Feature Box'),
		'view_item' => __('View Feature Box'),
		'search_items' => __('Search Feature Boxes'),
		'not_found' =>  __('No Feature Boxes found'),
		'not_found_in_trash' => __('No Feature Boxes found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Feature Boxes'

	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'featurebox'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => false,
		'menu_position' => 29,
		'supports' => array('title','page-attributes')
	);

	register_post_type('featurebox',$args);

	$labels = array(
		'name' => _x('Sponsors', 'post type general name'),
		'singular_name' => _x('Sponsor', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio'),
		'add_new_item' => __('Add New Sponsor'),
		'edit_item' => __('Edit Sponsor'),
		'new_item' => __('New Sponsor'),
		'view_item' => __('View Sponsor'),
		'search_items' => __('Search Sponsors'),
		'not_found' =>  __('No Sponsors found'),
		'not_found_in_trash' => __('No Sponsors found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Sponsors'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'sponsor'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_position' => 30,
		'supports' => array('title','page-attributes'),
		'taxonomies' => array('year')
	);

	register_post_type('sponsor',$args);

	$labels = array(
		'name' => _x('Important Dates', 'post type general name'),
		'singular_name' => _x('Date', 'post type singular name'),
		'add_new' => _x('Add New', 'portfolio'),
		'add_new_item' => __('Add New Date'),
		'edit_item' => __('Edit Date'),
		'new_item' => __('New Date'),
		'view_item' => __('View Date'),
		'search_items' => __('Search Dates'),
		'not_found' =>  __('No Important Dates found'),
		'not_found_in_trash' => __('No Dates found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Important Dates'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'date'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_position' => 31,
		'supports' => array('title','page-attributes')
	);

	register_post_type('date',$args);

	$labels = array(
		'name' => _x('Teams', 'post type general name'),
		'singular_name' => _x('Team', 'post type singular name'),
		'add_new' => _x('Create New', 'portfolio'),
		'add_new_item' => __('Create New Team'),
		'edit_item' => __('Edit Team'),
		'new_item' => __('New Team'),
		'view_item' => __('View Team'),
		'search_items' => __('Search Teams'),
		'not_found' =>  __('No Teams found'),
		'not_found_in_trash' => __('No Teams found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Teams'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'team'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_position' => 51,
		'supports' => array('title','page-attributes')
	);

	register_post_type('team',$args);

	$labels = array(
		'name' => _x('Films', 'post type general name'),
		'singular_name' => _x('Film', 'post type singular name'),
		'add_new' => _x('Create New', 'portfolio'),
		'add_new_item' => __('Create New Film'),
		'edit_item' => __('Edit Film'),
		'new_item' => __('New Film'),
		'view_item' => __('View Film'),
		'search_items' => __('Search Films'),
		'not_found' =>  __('No Films found'),
		'not_found_in_trash' => __('No Films found in Trash'),
		'parent_item_colon' => '',
		'menu_name' => 'Films'

	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'query_var' => true,
		'rewrite' => Array('slug'=>'film'),
		'capability_type' => 'post',
		'has_archive' => true,
		'hierarchical' => true,
		'menu_position' => 52,
		'supports' => array('title','page-attributes')
	);

	register_post_type('film',$args);
}
add_action('init', 'register_CPTs');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'site-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Secret Settings',
		'menu_title'	=> 'Secret Settings',
		'menu_slug' 	=> 'secret-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Messages',
		'menu_title'	=> 'Messages',
		'menu_slug' 	=> 'messages',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}


	pti_set_post_type_icon( 'featurebox', 'columns' );
	pti_set_post_type_icon( 'sponsor', 'usd' );
	pti_set_post_type_icon( 'date', 'calendar' );
	pti_set_post_type_icon( 'team', 'users' );
	pti_set_post_type_icon( 'film', 'video-camera' );