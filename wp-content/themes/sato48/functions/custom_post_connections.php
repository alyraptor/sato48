<?php

function my_connection_types() {
	
	$args = array(
		'name' => 'team_to_film',
		'from' => 'team',
		'to' => 'film',
		'cardinality' => 'one-to-many',
		'title' => array( 'from' => 'Films Created', 'to' => 'Created by' )
	);

	p2p_register_connection_type( $args );

	$args = array(
		'name' => 'team_managers',
		'from' => 'user',
		'to' => 'team',
		'reciprocal' => true,
		'title' => 'Team Managers'
	);

	p2p_register_connection_type( $args );

	$args = array(
		'name' => 'team_members',
		'from' => 'user',
		'to' => 'team',
		'reciprocal' => true,
		'title' => 'Team Members'
	);

	p2p_register_connection_type( $args );

}
add_action( 'p2p_init', 'my_connection_types' );