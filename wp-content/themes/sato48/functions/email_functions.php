<?php
//=========================
// Customize System Emails
//=========================

function wp_mail_from_name( $original_email_from ) {
    return 'SATO48';
}
add_filter( 'wp_mail_from_name', 'wp_mail_from_name' );

function mail_from_address( $original_email_address ) {
    return 'noreply@sato48.com';
}
add_filter( 'wp_mail_from', 'mail_from_address' );

function activation_email_subject( $text ) {
	return $text; //'Customize me: Your account needs activation.';
}
add_filter( 'wpmu_signup_user_notification_subject', 'activation_email_subject', 10, 4 );


if ( !function_exists('wp_new_user_notification') ) {

	function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {

		$user = new WP_User( $user_id );

		$user_login = stripslashes( $user->user_login );
		$user_email = stripslashes( $user->user_email );

		$message  = sprintf( __('New user registration on %s:'), get_option('blogname') ) . "\r\n\r\n";
		$message .= sprintf( __('Username: %s'), $user_login ) . "\r\n\r\n";
		$message .= sprintf( __('E-mail: %s'), $user_email ) . "\r\n";

		@wp_mail(
			get_option('admin_email'),
			sprintf(__('[%s] New User Registration'), get_option('blogname') ),
			$message
		);

		if ( empty( $plaintext_pass ) )
			return;

		$message  = sprintf( __("Thank you for registering at %s! Here's your log in information:"), get_option('blogname')) . "\r\n\r\n";
		$message .= wp_login_url() . "\r\n";
		$message .= sprintf( __('Username: %s'), $user_login ) . "\r\n";
		$message .= sprintf( __('Password: %s'), $plaintext_pass ) . "\r\n\r\n";
		$message .= sprintf( __('For support or questions, email %s'), 'jeffclinkenbeard@gmail.com' ) . "\r\n\r\n"; //get_option('admin_email')

		wp_mail(
			$user_email,
			sprintf( __('[%s] Your username and password'), get_option('blogname') ),
			$message
		);
	}
}