<?php

function must_be_logged_in() {
	// Pass false if login is not necessary
	// Check whether user is logged in, else display login/registration modal. (Currently, temporary redirect)
	if( !is_user_logged_in() ) :

		$location = home_url();
		wp_redirect( $location );
		exit;

	endif;
}

function insert_post( $type, $fields, $slug, $login = true ) {

	if( $login ) :

		must_be_logged_in();

	endif;

	switch ($type):
		case "team":

			$team_name = $fields['1'];

			$new_team = array(
				'post_title'	=> wp_strip_all_tags( $team_name ),
				'post_name'		=> $slug,
				'post_status'	=> 'publish',
				'post_type'		=> 'team'
			);

			$pid = wp_insert_post($new_team);

			return $pid;

			break;
		case "film":

			$team_id = $fields;

			$new_film = array(
				'post_title'	=> 'Untitled ' . $team_id,
				'post_name'		=> $slug,
				'post_status'	=> 'publish',
				'post_type'		=> 'film'
			);

			$pid = wp_insert_post($new_film);

			return $pid;

			break;
		default:

			break;	
	endswitch;
}

function new_post_connection ( $from, $to, $type = false, $login = true ) {

	if( $login ) :

		must_be_logged_in();

	endif;

	if( $type == false ) :

		$type = $_POST['connection_type'];
	
	endif;

	// Create connection
	$new_connection = p2p_type( $type )->connect( $from, $to, array('date' => current_time('mysql')) );

	return $new_connection;
}

function manage_user ($user_id, $user_meta) {

	if ( !empty( $_POST['update_user_nonce'] ) && wp_verify_nonce( $_POST['update_user_nonce'], 'update_user_'. get_current_user_id() ) ) {

		must_be_logged_in();

		if( !empty($user_meta['first_name']) && !empty($user_meta['last_name']) ):
			$display_name = $user_meta['first_name'] . ' ' . $user_meta['last_name'];
		endif;
		wp_update_user( array( 'ID' => get_current_user_id(), 'display_name' => $display_name ) );
		foreach($user_meta as $meta_key => $meta_value){
			update_user_meta($user_id, $meta_key, $meta_value);
		}
	}
}

function manage_post ($post_id, $post_type = false, $post_meta = false) {

	if( $post_meta != false ):
		foreach( $post_meta as $meta_key => $meta_value ):
			if( $post_type == 'team' ):

				if( $meta_value[0] == 'repeater' ):
					$field_key = $meta_value[1];
					$value = get_field($field_key, $post_id);
					// Check if editing or adding
					$row_count = 0;
					$row_num = '';
					foreach($value as $row):
						if( $row['gravity_id'] == $meta_value[2]['gravity_id'] ):
							$row_num = $row_count;
						endif;
						$row_count++;
					endforeach;
					$value[$row_num] = $meta_value[2];
					update_field( $field_key, $value, $post_id );
				elseif( $meta_key == 'slug' ):
					$post = array('ID' => $post_id, 'post_name' => $meta_value[0] );
					wp_update_post($post);
				else:
					$field_key = $meta_value[0];
					$value = $meta_value[1];
					update_field( $field_key, $value, $post_id );
				endif;

			elseif( $post_type == 'film' ):
				if( $meta_value[0] == 'repeater' ):
					$field_key = $meta_value[1];
					$value = get_field($field_key, $post_id);
					$value[] = $meta_value[2];
					update_field( $field_key, $value, $post_id );
				endif;
			endif;
		endforeach;
		return true;
	endif;

}

function manage_post2 ($post_id, $login = true) {

	if ( !empty( $_POST['update_post_nonce'] )  && wp_verify_nonce( $_POST['update_post_nonce'], 'update_post_'. get_the_ID() ) ) {
		if( $login ) :
			// Pass false if login is not necessary
			// Check whether user is logged in, else display login/registration modal. (Currently, temporary redirect)
			if( !is_user_logged_in() ) :

				$location = home_url();
				wp_redirect( $location );
				exit;

			endif;

		endif;

		$type = get_post_type();

		if( ( ( $type == 'team') && ( is_manager() ) ) || ( ( $type == 'film' ) && ( user_made_film() ) ) ) :
			if( !empty( $_POST['action'] )   && ( $_POST['action'] == 'manage_post') ) :

				$post_information = array(
				    'ID' => $post_id,
				    'post_type' => $type
				);

				if( $type == 'team' ):
					if( !empty( $_POST['team_name'] )   ):
						$update = update_post_meta( $post_id, 'team_name', $_POST['team_name'] );
					endif;

					$location = get_permalink( $post_id );
					wp_redirect( $location );
					exit;
				elseif( $type == 'film' ):
					if( !empty( $_POST['film_name'] )   ):
						$update = update_post_meta( $post_id, 'film_name', $_POST['film_name'] );
					endif;
					if( !empty( $_POST['film_description'] )   ):
						$update = update_post_meta( $post_id, 'film_description', $_POST['film_description'] );
					endif;

				endif;

			elseif( !empty( $_POST['action'] )   && ( $_POST['action'] == 'manage_nominations') ) :
				// Get team post id for the specific form
				$roles = explode(",", get_field('nominations_list', 'option'));
				foreach( $roles as $role_space ):
					$role = str_replace(' ', '_', $role_space);
					$meta[$role] = $_POST[$role];
				endforeach;
				// unset($meta['save_nominations'], $meta['action'], $meta['user_id'], $meta['update_post_nonce'], $meta['_wp_http_referer']); 

				update_post_meta($post_id, 'nominations', $meta);

			elseif( !empty( $_POST['action'] )  && ( $_POST['action'] == 'manage_crew') ) :

				if((empty($_POST['email'])) || (empty($_POST['first_name'])) || (empty($_POST['last_name'])) || (empty($_POST['address'])) || (empty($_POST['city'])) || (empty($_POST['state'])) || (empty($_POST['zip'])) || (empty($_POST['phone']))):
					global $validation;
					$validation = 'fail';
				else:

				$user = get_user_by( 'email', $_POST['email'] );
				// print_r($user);
				if(!empty($user)): $link = $user->ID; else: $link = ''; endif;

				$field_key = "field_550dbeb0fb136";
				$value = get_field($field_key, $post_id);

				$value[] = array(
					'acf_fc_layout' => 'cast_crew_member',
					'user' => $link,
					'email' => $_POST['email'],
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'address' => $_POST['address'],
					'address_2' => $_POST['address2'],
					'city' => $_POST['city'],
					'state' => $_POST['state'],
					'zip' => $_POST['zip'],
					'phone' => $_POST['phone'],
					'crew_id' => $_POST['crew_id']
				);
				update_field( $field_key, $value, $post_id );

				endif;

			elseif( !empty( $_POST['action'] )   && ( $_POST['action'] == 'remove_crew') ) :
				$field_key = "field_550d2fe98863c";
				$value = get_field($field_key);

				// print_r($_POST);
				foreach($value as $array):
					// print_r($array);

					// if( $array['crew_id'] == $_POST['crew_id'] ):
					// 	// $thisone = $array;
					// endif;

				endforeach;

				// unset($value[$thisone]);

				// update_field( $field_key, $value, $post_id );

			endif;

		else :

		$location = get_permalink( $post_id );
		wp_redirect( $location );
		exit;

		endif;
	}
}