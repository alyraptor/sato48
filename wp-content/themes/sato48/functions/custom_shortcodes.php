<?php

function registration_price() {
    
	$price = '$' . get_field('registration_fee', 'option');
    return $price;
}
add_shortcode('reg_fee', 'registration_price');