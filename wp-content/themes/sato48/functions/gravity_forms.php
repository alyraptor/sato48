<?php
//=========================
// Merge Tags
//=========================

add_filter('gform_field_value_address', create_function("", '$value = populate_usermeta(\'address\'); return $value;' ));

add_filter('gform_field_value_address2', create_function("", '$value = populate_usermeta(\'address2\'); return $value;' ));

add_filter('gform_field_value_city', create_function("", '$value = populate_usermeta(\'city\'); return $value;' ));

add_filter('gform_field_value_state', create_function("", '$value = populate_usermeta(\'state\'); return $value;' ));

add_filter('gform_field_value_zip', create_function("", '$value = populate_usermeta(\'zip\'); return $value;' ));

add_filter('gform_field_value_country', create_function("", '$value = populate_usermeta(\'country\'); return $value;' ));

add_filter('gform_field_value_phone', create_function("", '$value = populate_usermeta(\'phone\'); return $value;' ));

add_filter('gform_field_value_email', create_function("", '$value = populate_usermeta(\'user_email\'); return $value;' ));

function populate_usermeta($meta_key){
	global $current_user;
	return $current_user->__get($meta_key);
}

function populate_post_registration_fee($value) {
	$price = get_field('registration_fee', 'option');
	return $price;
}
add_filter('gform_field_value_registration_fee', 'populate_post_registration_fee');

function populate_post_registration_label($value) {
	$label = get_field('registration_label', 'option');
	return $label;
}
add_filter('gform_field_value_registration_label', 'populate_post_registration_label');

function populate_post_registration_year($value) {
	$year = get_field('registration_year', 'option');
	return $year;
}
add_filter('gform_field_value_registration_year', 'populate_post_registration_year');

function change_duplicate_message($message, $form){
  $message = "This team name has already been registered this year.";
  return $message;
}
add_filter("gform_duplicate_message_2", "change_duplicate_message", 10, 2);

//=========================
// Store as Team
//=========================

function register_team_from_form($entry, $form) {

	if($entry['form_id'] == '2'):

		// Get and update team count
		$team_count = get_field('registration_count', 'option'); $team_count++;
		update_field('registration_count', $team_count, 'options');

		$team_year = $entry['28'];
		$team_count = str_pad($team_count, 3, '0', STR_PAD_LEFT);
		$team_number = $team_year . '-' . $team_count;

		// Insert the new Team and Film posts
		$team_post_id = insert_post('team', $entry, $team_number, false);
		$film_post_id = insert_post('film', $team_number, $team_number, false);

		// Now add the team's metadata
		$user_id = get_current_user_id();
		$date = $team_year . '0101';
		if( !empty( $entry['10.1'] ) && ( $entry['10.1'] ) == "Yes" ) : $under_18 = true; else : $under_18 = false; endif;

		if( !empty($entry['16']) && ($entry['16'] == 'Pay with Paypal') ):
			$payment_type = 'paypal';
		elseif( !empty($entry['16']) && ($entry['16'] == 'Pay with cash (offline payment)') ):
			$payment_type = 'cash';
		endif;

		$post_meta = array(
			'slug' => array( $team_number ),
			'team_name' => array( 'field_54a717b7e333e', $entry['1'] ),
			'owner' => array( 'field_54a71871e333f', $user_id ),
			'years' => array(
				'repeater',
				'field_54a718a7e3340',
				array(
					'team_name' => $entry['1'],
					'team_id' => $team_count,
					'date' => $date,
					'paid' => false,
					'payment_type' => $payment_type,
					'gravity_id' => $entry['id'],
					'under_18' => $under_18,
					'film_connection' => $film_post_id,
					'members' => array( 1 )
				)
			)
		);
		manage_post($team_post_id, 'team', $post_meta);

		// Add Film connection to Team
		$new_connection = new_post_connection( $team_post_id, $film_post_id, 'team_to_film', false );

		// Add user to Members list in first year
		$years = get_field('years', $team_post_id);
		$year = array_keys($years);
		$year = end($year);

		$meta_key = 'years_' . $year . '_members';

		$user = get_user_by('id', $user_id);
		$field[] = array(
			'user' => array(
				'ID' => $user->ID
			)
		);

		update_field($meta_key, $field, $team_post_id);

		// Also make the team registrant a manager and a member
		new_post_connection( $user_id, $team_post_id, 'team_members' );
		new_post_connection( $user_id, $team_post_id, 'team_managers' );
	endif;

}
add_action("gform_post_submission", "register_team_from_form", 10, 2);

//=========================
// Verify Team Paypal
//=========================

function verify_team_paypal($entry, $config, $transaction_id, $amount) {

	global $wpdb;
	$entry_id = $entry['id'];

	// Get team post id for the specific form
	$result = $wpdb->get_results($wpdb->prepare( 
		"
		SELECT *
		FROM wp_postmeta
		WHERE meta_key LIKE %s
			AND meta_value = %s
		",
		'years_%_gravity_id',
		$entry_id
	));

	$team_post_id = $result[0]->post_id;

	$repeater = get_field('years', $team_post_id);

	// Find the correct row in the post
	foreach($repeater as $row):
		if ($row['gravity_id'] == $entry_id):
			$post_row = $row;
		endif;
	endforeach;

	// Set new meta
	$post_row['paid'] = 1;
	$post_meta = array(
		'years' => array(
			'repeater',
			'field_54a718a7e3340',
			$post_row
		)
	);

	manage_post($team_post_id, 'team', $post_meta);

}
add_action("gform_paypal_fulfillment", "verify_team_paypal", 10, 4);

//=========================
// Paypal Change Status
//=========================

function paypal_change_status($config, $entry, $status, $transaction_id, $subscriber_id, $amount, $pending_reason, $reason) {

	global $wpdb;
	$entry_id = $entry['id'];

	// Get team post id for the specific form
	$result = $wpdb->get_results($wpdb->prepare( 
		"
		SELECT *
		FROM wp_postmeta
		WHERE meta_key LIKE %s
			AND meta_value = %s
		",
		'years_%_gravity_id',
		$entry_id
	));

	$team_post_id = $result[0]->post_id;
	$repeater = get_field('years', $team_post_id);

	// Find the correct row in the post
	foreach($repeater as $row):
		if ($row['gravity_id'] == $entry_id):
			$post_row = $row;
		endif;
	endforeach;

	// Set new meta
	$post_row['paypal_status'] = $status;
	$post_meta = array(
		'years' => array(
			'repeater',
			'field_54a718a7e3340',
			$post_row
		)
	);

	manage_post($team_post_id, 'team', $post_meta);

}
add_action("gform_post_payment_status", "paypal_change_status", 10, 8);


function add_bcc($form){

    //creating list of emails based on fields on the form
    $bcc  = 'jeffclinkenbeard@gmail.com' . ","; //player 1 email
    $bcc .= 'aly.d.richardson@gmail.com'; //player 2 email

    //setting notification BCC field to the list of fields
    $form["notification"]["bcc"] = $bcc;

    //returning modified form object
    return $form;
}
add_filter("gform_pre_submission_filter_2", "add_bcc");

//=========================
// Confirmation Page
//=========================

/**
* Better Pre-submission Confirmation
* http://gravitywiz.com/2012/08/04/better-pre-submission-confirmation/
*/
class GWPreviewConfirmation {

	private static $lead;

	public static function init() {
		add_filter( 'gform_pre_render', array( __class__, 'replace_merge_tags' ) );
	}

	public static function replace_merge_tags( $form ) {

		$current_page = isset(GFFormDisplay::$submission[$form['id']]) ? GFFormDisplay::$submission[$form['id']]['page_number'] : 1;
		$fields = array();

		// get all HTML fields on the current page
		foreach($form['fields'] as &$field) {

			// skip all fields on the first page
			if(rgar($field, 'pageNumber') <= 1)
				continue;

			$default_value = rgar($field, 'defaultValue');
			preg_match_all('/{.+}/', $default_value, $matches, PREG_SET_ORDER);
			if(!empty($matches)) {
				// if default value needs to be replaced but is not on current page, wait until on the current page to replace it
				if(rgar($field, 'pageNumber') != $current_page) {
					$field['defaultValue'] = '';
				} else {
					$field['defaultValue'] = self::preview_replace_variables($default_value, $form);
				}
			}

			// only run 'content' filter for fields on the current page
			if(rgar($field, 'pageNumber') != $current_page)
				continue;

			$html_content = rgar($field, 'content');
			preg_match_all('/{.+}/', $html_content, $matches, PREG_SET_ORDER);
			if(!empty($matches)) {
				$field['content'] = self::preview_replace_variables($html_content, $form);
			}

		}

		return $form;
	}

	/**
	* Adds special support for file upload, post image and multi input merge tags.
	*/
	public static function preview_special_merge_tags($value, $input_id, $merge_tag, $field) {
		
		// added to prevent overriding :noadmin filter (and other filters that remove fields)
		if( ! $value )
			return $value;
		
		$input_type = RGFormsModel::get_input_type($field);
		
		$is_upload_field = in_array( $input_type, array('post_image', 'fileupload') );
		$is_multi_input = is_array( rgar($field, 'inputs') );
		$is_input = intval( $input_id ) != $input_id;
		
		if( !$is_upload_field && !$is_multi_input )
			return $value;

		// if is individual input of multi-input field, return just that input value
		if( $is_input )
			return $value;
			
		$form = RGFormsModel::get_form_meta($field['formId']);
		$lead = self::create_lead($form);
		$currency = GFCommon::get_currency();

		if(is_array(rgar($field, 'inputs'))) {
			$value = RGFormsModel::get_lead_field_value($lead, $field);
			return GFCommon::get_lead_field_display($field, $value, $currency);
		}

		switch($input_type) {
		case 'fileupload':
			$value = self::preview_image_value("input_{$field['id']}", $field, $form, $lead);
			$value = self::preview_image_display($field, $form, $value);
			break;
		default:
			$value = self::preview_image_value("input_{$field['id']}", $field, $form, $lead);
			$value = GFCommon::get_lead_field_display($field, $value, $currency);
			break;
		}

		return $value;
	}

	public static function preview_image_value($input_name, $field, $form, $lead) {

		$field_id = $field['id'];
		$file_info = RGFormsModel::get_temp_filename($form['id'], $input_name);
		$source = RGFormsModel::get_upload_url($form['id']) . "/tmp/" . $file_info["temp_filename"];

		if(!$file_info)
			return '';

		switch(RGFormsModel::get_input_type($field)){

			case "post_image":
				list(,$image_title, $image_caption, $image_description) = explode("|:|", $lead[$field['id']]);
				$value = !empty($source) ? $source . "|:|" . $image_title . "|:|" . $image_caption . "|:|" . $image_description : "";
				break;

			case "fileupload" :
				$value = $source;
				break;

		}

		return $value;
	}

	public static function preview_image_display($field, $form, $value) {

		// need to get the tmp $file_info to retrieve real uploaded filename, otherwise will display ugly tmp name
		$input_name = "input_" . str_replace('.', '_', $field['id']);
		$file_info = RGFormsModel::get_temp_filename($form['id'], $input_name);

		$file_path = $value;
		if(!empty($file_path)){
			$file_path = esc_attr(str_replace(" ", "%20", $file_path));
			$value = "<a href='$file_path' target='_blank' title='" . __("Click to view", "gravityforms") . "'>" . $file_info['uploaded_filename'] . "</a>";
		}
		return $value;

	}

	/**
	* Retrieves $lead object from class if it has already been created; otherwise creates a new $lead object.
	*/
	public static function create_lead( $form ) {
		
		if( empty( self::$lead ) ) {
			self::$lead = GFFormsModel::create_lead( $form );
			self::clear_field_value_cache( $form );
		}
		
		return self::$lead;
	}

	public static function preview_replace_variables( $content, $form ) {

		$lead = self::create_lead($form);

		// add filter that will handle getting temporary URLs for file uploads and post image fields (removed below)
		// beware, the RGFormsModel::create_lead() function also triggers the gform_merge_tag_filter at some point and will
		// result in an infinite loop if not called first above
		add_filter('gform_merge_tag_filter', array('GWPreviewConfirmation', 'preview_special_merge_tags'), 10, 4);

		$content = GFCommon::replace_variables($content, $form, $lead, false, false, false);

		// remove filter so this function is not applied after preview functionality is complete
		remove_filter('gform_merge_tag_filter', array('GWPreviewConfirmation', 'preview_special_merge_tags'));

		return $content;
	}
	
	public static function clear_field_value_cache( $form ) {
		
		if( ! class_exists( 'GFCache' ) )
			return;
			
		foreach( $form['fields'] as &$field ) {
			if( GFFormsModel::get_input_type( $field ) == 'total' )
				GFCache::delete( 'GFFormsModel::get_lead_field_value__' . $field['id'] );
		}
		
	}

}
GWPreviewConfirmation::init();