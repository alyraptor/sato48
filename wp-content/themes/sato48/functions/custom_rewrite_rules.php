<?php

//=========================
// Duplicate plugin
//=========================

// function login_rewrite_rules() {
// 	global $wp_rewrite;

// 	add_rewrite_rule( "^login/?$", "/sato48/wp-login.php", 'top' );
// 	add_rewrite_rule( "^register/?$", "/sato48/wp-login.php?action=register", 'top' );
// 	add_rewrite_rule( "^forgot/?$", "/sato48/wp-login.php?action=lostpassword", 'top' );
// }
// add_filter('init', 'login_rewrite_rules');

//=========================
// Custom Endpoints
//=========================

function manage_endpoint() {

	add_rewrite_endpoint( 'manage', EP_PERMALINK );

}
add_filter('init', 'manage_endpoint');

//=========================
// Author/User Rules
//=========================

// Define the author levels you want to use
global $custom_author_levels;
$custom_author_levels = array( 'user' );

// On init, add a new author_level rewrite tag and add it to the author_base property of wp_rewrite
function author_rewrite()
{
	global $wp_rewrite;
	$author_levels = $GLOBALS['custom_author_levels'];

	// Define the tag and use it in the rewrite rule
	add_rewrite_tag( '%author_level%', '(' . implode( '|', $author_levels ) . ')' );
	$wp_rewrite->author_base = '%author_level%';

}
add_action( 'init', 'author_rewrite' );


// The previous function creates extra author_name rewrite rules that are unnecessary. This function tests for and removes them
function new_author_rewrite_rules( $author_rewrite_rules )
{
	foreach ( $author_rewrite_rules as $pattern => $substitution ) {
		if ( FALSE === strpos( $substitution, 'author_name' ) ) {
			unset( $author_rewrite_rules[$pattern] );
		}
	}
	return $author_rewrite_rules;
}
add_filter( 'author_rewrite_rules', 'new_author_rewrite_rules' );

function new_author_link( $link, $author_id )
{
	if ( 100 > $author_id ) {
		$author_level = 'ninja';
	} else {
		$author_level = 'user';
	}
	$link = str_replace( '%author_level%', $author_level, $link );
	return $link;
}
add_filter( 'author_link', 'new_author_link', 10, 2 );

//=========================
// Logout URL
//=========================

function logout_rewrite_rules() {
	global $wp_rewrite;

	add_rewrite_rule( "^logout/(.*)$", "/wp-login.php?action=logout&redirect_to=/&_wpnonce=$1", 'top' );
	// add_rewrite_rule( "^login/?$", "/sato48/wp-login.php", 'top' );
	// add_rewrite_rule( "^register/?$", "/sato48/wp-login.php?action=register", 'top' );
	// add_rewrite_rule( "^forgot/?$", "/sato48/wp-login.php?action=lostpassword", 'top' );
}
add_filter('init', 'logout_rewrite_rules');