<?php

//=========================
// Manage Redirect
//=========================

function manage_include( $template )
{
	if ( is_single() ) {

		global $wp_query;
		global $post;
		$user = get_current_user_id();

		if( isset ( $wp_query->query_vars['manage'] ) ) {

			if( $post->post_type == 'team' ):
				$post_id = $post->ID;
				$new_template = locate_template( array( '/template-team-manage.php' ) );
			elseif( $post->post_type == 'film' ):
				$post_id = film_team_lookup();
				$new_template = locate_template( array( '/template-film-manage.php' ) );
			endif;

			if ( is_manager($user, $post_id) ) {

				if ( '' != $new_template ) {
					return $new_template ;
				}
			}
		}
	}

	return $template;
}
add_action( 'template_include', 'manage_include' );

//=========================
// Author/User Redirect
//=========================

function author_include( $template )
{
	if ( is_author() ):

		global $wp_query;

		if( isset ( $wp_query->query_vars['author_level'] ) ):

			if ( $wp_query->query_vars['author_level'] == 'user' ) :

				$new_template = locate_template( array( '/user.php' ) );
				return $new_template;

			else:

				return $template;

			endif;
		
		endif;

	endif;

	return $template;
}
add_action( 'template_include', 'author_include' );
