<?php wp_footer(); ?>

	</div>

	<script src="<?php bloginfo('template_directory') ?>/assets/js/main.js"></script>

	<?php if ( ENVIRONMENT != 'production' ) { ?>
		<script type='text/javascript'>
			(function (d, t) {
				var bh = d.createElement(t), s = d.getElementsByTagName(t)[0],
					apiKey = 'API KEY HERE';
				bh.type = 'text/javascript';
				bh.src = '//www.bugherd.com/sidebarv2.js?apikey=' + apiKey;
				s.parentNode.insertBefore(bh, s);
			})(document, 'script');
		</script>
	<?php } ?>

	<?php if (is_page_template( 'template-event.php' )) : ?>
		<script>jQuery(".clock").TimeCircles();</script>
	<?php endif; ?>

<footer id="footer">
	<div class="container">
		<div class="content-wrap sato-footer">
			<p>SATO48 "Making Movies Better &amp; Making Better Movies" &copy; <?php echo date('Y'); ?></p>
		</div>
	</div>
</body>
</html>