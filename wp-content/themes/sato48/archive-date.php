<?php get_header(); ?>

<div class="content-wrap">
	<div class="breadcrumbs">
		<?php theme_breadcrumbs(); ?>
	</div>
</div>

<?php

	$args = array(
		'post_type' => 'date',
		'meta_key' => 'start_date',
		'meta_type' => 'DATE',
		'orderby' => 'meta_value',
		'order' => 'ASC'
	);

	$the_query = new WP_Query( $args );

?>

<div class="container">

	<div class="content-wrap">

		<div class="row date-row">

			<div class="date-titles">
				<h6>2015</h6>
			</div>

			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

				<a class="date-link col-md-3" href="<?php the_permalink(); ?>">
		
					<div class="date-box">

						<h3><?php the_field( 'event_title' ); ?></h3>

						<?php $small_date = DateTime::createFromFormat('l, F j, Y', get_field('start_date')); ?>

						<div class="date-inner">
							<div class="date-icon">
								<h4><?php echo $small_date->format('M'); ?></h4>
								<h3><?php echo $small_date->format('j'); ?></h3>
							</div>

							<?php if ( get_field( 'end_date' ) ) : ?>

							<?php $small_date_end = DateTime::createFromFormat('l, F j, Y', get_field('end_date')); ?>

							<p></p>
							<div class="date-icon">
								<h4><?php echo $small_date_end->format('M'); ?></h4>
								<h3><?php echo $small_date_end->format('j'); ?></h3>
							</div>

						<?php endif; ?>

						</div>
							
					</div>

				</a>

			<?php endwhile; ?>

		</div>

	</div>

</div>


<!--
<?php 

$years = years_array(2004, 'present'); // Returns an array (of arrays) of $years from start to finish

while ( $the_query->have_posts() ) : $the_query->the_post(); 

	$sponsored_years = get_field( 'years' ); // An array of years from the current sponsor

	foreach ( $sponsored_years as $y ) {

		if ( in_array( $y, $years ) ) { // Add each instance of sponsoring to array

			$name = get_field( 'sponsor_name' ); // Adds the name for easy alphabetical sorting

			$output_years[$y][$name] = "<a href=\"" . get_permalink() . "\">" . get_field( 'sponsor_name' ) . "</a>";

		}

	}

	krsort($output_years); // Sort Years output in Descending order

endwhile;

?>

<div class="container">

	<div class="row spon-row spon-page">

		<?php

			foreach ( $output_years as $year => $sponsors_array ) { // Echo list of years and their sponsors, with links
				echo '<div class="spon-year-block">';
				echo '<h3 class="spon-year-titles">' . $year . '</h3>';
				echo '<ul class="year-list">';

				ksort($sponsors_array);

				foreach ( $sponsors_array as $sponsor ) {
					echo '<li>' . $sponsor . '</li>';
				}

				echo '</ul>';
				echo '</div>';
			}

		?>

	</div>

</div> -->

<?php get_footer(); ?>