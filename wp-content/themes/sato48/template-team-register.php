<?php 
/*
Template Name: Team Register
*/
if( !is_user_logged_in() ) :
	$location = wp_login_url();
		wp_redirect( $location );
		exit;
endif;
get_header(); ?>

<div class="container">
	<div class="content-wrap">
		<div class="form-wrap">
			<div id="postbox">
				
			<?php gravity_form( 2, $display_title = false, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false ); ?>


			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>