<?php $current_year = get_field('site_year', 'option');
get_header(); ?>

<div class="content-wrap">
	<div class="breadcrumbs">
		<?php theme_breadcrumbs(); ?>
	</div>
</div>

<?php

	$args = array(
		'post_type' => 'sponsor',
		'meta_query' => array(
			array(
				'key' => 'years',
				'value' => $current_year,
				'compare' => 'LIKE'
			)
		),
		'order' => 'DESC'
	);

	$the_query = new WP_Query( $args );

?>

<div class="container">
	<div class="content-wrap">
		<div class="row spon-row spon-page">

			<div class="spon-titles">
				<h3>This Year</h3>
			</div>

			<div class="spon-flex">
				<?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			
					<div class="spon-box spon-page">

						<div class="spon-inner" style="background-image: url('<?php the_field( 'sponsor_image' ); ?>');"></div>
						<div class="sato-gray"></div>
						<h2><?php the_field( 'sponsor_name' ); ?></h2>
						<a href="<?php the_permalink(); ?>"></a>	
					</div>

				<?php endwhile; else: ?>
				<span class="no-sponsors"><p>No sponsors to show at this time.</p></span>
				<?php endif; ?>

			</div>
		</div>
		<?php 

		$years = years_array(2004, 'present'); // Returns an array (of arrays) of $years from start to finish

		while ( $the_query->have_posts() ) : $the_query->the_post();

			$sponsored_years = get_field( 'years' ); // An array of years from the current sponsor

			foreach ( $sponsored_years as $y ) {

				if ( in_array( $y, $years ) ) { // Add each instance of sponsoring to array

					$name = get_field( 'sponsor_name' ); // Adds the name for easy alphabetical sorting

					$output_years[$y][$name] = "<a href=\"" . get_permalink() . "\">" . get_field( 'sponsor_name' ) . "</a>";

				}

			}

			krsort($output_years); // Sort Years output in Descending order

		endwhile;

		?>

		<div class="row spon-row spon-page">

			<?php

				foreach ( $output_years as $year => $sponsors_array ) { // Echo list of years and their sponsors, with links
					echo '<div class="spon-year-block">';
					echo '<h3 class="spon-year-titles">' . $year . '</h3>';
					echo '<ul class="year-list">';

					ksort($sponsors_array);

					foreach ( $sponsors_array as $sponsor ) {
						echo '<li>' . $sponsor . '</li>';
					}

					echo '</ul>';
					echo '</div>';
				}

			?>

		</div>
	</div>
</div>

<?php get_footer(); ?>